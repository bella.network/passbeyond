module gitlab.com/bella.network/passbeyond

go 1.20

require (
	github.com/crewjam/saml v0.4.13
	golang.org/x/crypto v0.13.0 // indirect
)

require (
	github.com/google/uuid v1.3.1
	github.com/jonboulle/clockwork v0.4.0 // indirect
	github.com/russellhaering/goxmldsig v1.4.0 // indirect
)

require (
	github.com/beevik/etree v1.2.0 // indirect
	github.com/crewjam/httperr v0.2.0 // indirect
	github.com/go-ldap/ldap/v3 v3.4.6
	github.com/go-sql-driver/mysql v1.7.1
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/mattermost/xml-roundtrip-validator v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20221128193559-754e69321358 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.5 // indirect
	gopkg.in/yaml.v3 v3.0.1
)

require gopkg.in/yaml.v2 v2.2.8
