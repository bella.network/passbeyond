package main

import (
	"log"
	"net/http"
	"time"
)

const (
	// Path to a local copy of IDP metadata XML
	idpMetadataPath = "/var/lib/passbeyond/idp-metadata.xml"

	// Path to a local copy of the public key for SAML authentication
	samlPublicKeyPath = "/var/lib/passbeyond/saml-publickey.pem"

	// Path to a local copy of the private key for SAML authentication
	samlPrivateKeyPath = "/var/lib/passbeyond/saml-privatekey.pem"

	// commitHash is the commit hash of the current build
	commitHash = "00000000"

	// buildDate is the date of the current build
	buildDate = "2018-04-15"

	// programVersion is the version of the program based on the last tag
	programVersion = "0"
)

func init() {
	http.DefaultClient.Timeout = time.Minute * 10
}

func main() {
	// Hello message of starting program
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("Starting PassBeyond %s (%s) build on %s", programVersion, commitHash, buildDate)

	// Read config file
	log.Println("Reading config")
	readConfig()

	// Parse list of internal IPs
	log.Println("Parsing internal IPs")
	parseInternalIPList()

	// initiate MySQL connection
	log.Println("Connecting to database")
	initiateMySQL()

	// check if database schema exists
	schema := initiateMySQLSchema()
	if !schema {
		log.Fatalln("Database schema does not exist")
	}

	// perform database schema upgrade if needed
	checkSchemaUpdate()

	// Initiate application mappings cache
	log.Println("Renewing application cache")
	cacheApplications.RenewCache()

	// load and parse themes for serving to client
	log.Println("Loading themes into parser")
	if err := loadTemplates(); err != nil {
		log.Fatalln("Error parsing themes", err.Error())
	}

	// initiate LDAP connection
	log.Println("Connecting to LDAP")
	err := initiateLDAP()
	if err != nil {
		log.Fatalln("Error connecting to LDAP", err.Error())
	}

	// start the main page server for login and service overview
	log.Println("Starting main page server")
	go mainPageServer()

	// Start proxy server service for page proxy
	log.Println("Starting reverse proxy server")
	go httpProxyServer()

	// Listen for SIGUSR1
	go listenForReload()

	// Start periodic session usage update
	go func() {
		for {
			time.Sleep(time.Minute)
			sessionLastUsed.GC()
		}
	}()

	// start periodic session cleanup
	go func() {
		time.Sleep(time.Second * 15)
		for {
			session.GC()
			time.Sleep(time.Minute * 60)
		}
	}()

	// start periodic user cleanup
	go func() {
		time.Sleep(time.Second * 15)
		for {
			users.GC()
			time.Sleep(time.Minute)
			users.GCDatabase()
			time.Sleep(time.Hour)
		}
	}()

	// Endless wait to serve http and https servers
	select {}
}
