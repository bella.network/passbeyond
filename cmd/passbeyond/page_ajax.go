package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// ajaxHandler is used to handle AJAX requests to obtain user and instance data
func ajaxHandler(w http.ResponseWriter, r *http.Request) {
	// Check CORS
	// Check if origin is allowed
	if r.Header.Get("Origin") != "" {
		if !stringInSlice(r.Header.Get("Origin"), config.AuthServer.APIAllowedOrigins) {
			w.WriteHeader(http.StatusForbidden)
			_ = json.NewEncoder(w).Encode(map[string]string{"error": "origin not allowed"})
			return
		}
	}

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.WriteHeader(http.StatusOK)
		return
	}

	// determine path like /ajax/userinfo
	path := strings.TrimPrefix(r.URL.Path, "/ajax")

	// Check if session is valid
	thisSessionID, err := session.CheckValidHTTPSession(r)
	if err != nil || thisSessionID == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		_ = json.NewEncoder(w).Encode(map[string]string{"error": "unknown session"})
		return
	}

	// Get DN from session
	dn := session.GetUserDNFromID(thisSessionID)
	if dn == "" {
		w.WriteHeader(http.StatusNotFound)
		_ = json.NewEncoder(w).Encode(map[string]string{"error": "user not found"})
		return
	}

	// Provide access to user specific information for third-party applications
	switch path {
	case "/self/info":
		// Get user info
		userinfo, err := ldapFetchUserByDN(dn)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_ = json.NewEncoder(w).Encode(map[string]string{"error": "user not found"})
			return
		}

		type userInfoResponse struct {
			Name           string    `json:"name"`
			Mail           string    `json:"mail"`
			UPN            string    `json:"upn"`
			SAMAccountName string    `json:"sAMAccountName"`
			PwdLastSet     time.Time `json:"pwdLastSet"`
			Groups         []string  `json:"groups"`
		}

		// get users groups and convert ID to name
		groupIDs := users.GetGroups(thisSessionID)
		groupNames := []string{}
		for _, groupID := range groupIDs {
			name, err := users.GetGroupName(groupID)
			if err != nil {
				log.Println("Error getting group name for group ID ", groupID, err.Error())
				name = strconv.FormatUint(groupID, 10)
			}
			groupNames = append(groupNames, name)
		}

		// Return user info
		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(userInfoResponse{
			Name:           userinfo.CN,
			Mail:           userinfo.Mail,
			UPN:            userinfo.UserPrincipalName,
			SAMAccountName: userinfo.SAMAccountName,
			PwdLastSet:     userinfo.PwdLastSet,
			Groups:         groupNames,
		})
		return

	case "/self/groups":
		// get all users groups using LDAP
		w.Header().Set("Content-Type", "application/json")
		groups, err := ldapFetchAllUsersGroupsByDN(dn)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_ = json.NewEncoder(w).Encode(map[string]string{"error": "user not found"})
			return
		}

		_ = json.NewEncoder(w).Encode(groups)
		return

	case "/self/image":
		userinfo, err := ldapFetchUserByDN(dn)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_ = json.NewEncoder(w).Encode(map[string]string{"error": "user not found"})
			return
		}

		// determine MIME of image
		mime := http.DetectContentType(userinfo.ThumbnailPhoto)

		// Set headers
		w.Header().Set("Content-Type", mime)
		w.Header().Set("Content-Length", strconv.Itoa(len(userinfo.ThumbnailPhoto)))

		// Write image data
		_, err = w.Write(userinfo.ThumbnailPhoto)
		if err != nil {
			log.Println("Error writing image data", err.Error())
		}

		return

	default:
		// unknown backend path, return 404
		w.WriteHeader(http.StatusNotFound)
		_ = json.NewEncoder(w).Encode(map[string]string{"error": "unknown backend path"})
	}
}
