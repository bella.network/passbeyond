package main

import (
	"bytes"
	"context"
	"crypto/rsa"
	"encoding/xml"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/crewjam/saml"
	"github.com/crewjam/saml/samlsp"
)

var samlSP *samlsp.Middleware
var apiAllowedOriginDomains []string

const sessionCookieName = "__passbeyond-session"

func mainPageServer() {
	// Only allow from list of allowed domains
	apiAllowedOriginDomains = append(apiAllowedOriginDomains, config.AuthServer.APIAllowedOrigins...)
	// Are there additional domains to allow?

	keyPair, err := samlLoadKeyPair(samlKeySettings{
		PrivateKeyPath:         config.SAML.PrivateKey,
		PublicKeyPath:          config.SAML.PublicKey,
		FallbackPrivateKeyPath: samlPrivateKeyPath,
		FallbackPublicKeyPath:  samlPublicKeyPath,
		FallbackDomain:         config.BaseDomain,
	})
	if err != nil {
		log.Println("Error loading key pair ", err.Error())
		return
	}

	// check if IDP metadata is available
	if config.SAML.IdpMetadataURL == "" {
		panic("IDP metadata URL is not set")
	}

	// check if local IDP metadata file exists
	var idpMetadata *saml.EntityDescriptor
	if _, err := os.Stat(idpMetadataPath); err == nil {
		metadataContent, err := os.ReadFile(idpMetadataPath)
		if err != nil {
			log.Printf("Can't read IDP Metadata file locally: %s", err.Error())
			panic(err)
		}
		idpMetadata, err = samlsp.ParseMetadata(metadataContent)
		if err != nil {
			panic(err)
		}
	} else {
		idpMetadataURL, err := url.Parse(config.SAML.IdpMetadataURL)
		if err != nil {
			panic(err) // TODO handle error
		}

		idpMetadata, err = samlsp.FetchMetadata(context.Background(), http.DefaultClient, *idpMetadataURL)
		if err != nil {
			panic(err) // TODO handle error
		}
	}

	// If metadata cache is enabled periodically refetch metadata file
	if config.SAML.CacheMetadata {
		go func() {
			for {
				log.Println("Checking if new IDP metadata file is available")
				// check if local copy of metadata is available
				new, err := samlDownloadMetadataFile(config.SAML.IdpMetadataURL)
				if err != nil {
					log.Printf("Error downloading new version of IDP metadata file: %s", err.Error())
				}

				// if a new version is available, read file and replace currently used IDP Metadata information
				if new {
					log.Println("Found new IDP metadata file - Replacing currently used one")
					metadataContent, err := os.ReadFile(idpMetadataPath)
					if err != nil {
						log.Printf("Can't read IDP Metadata file locally: %s", err.Error())
						panic(err)
					}
					idpMetadata, err = samlsp.ParseMetadata(metadataContent)
					if err != nil {
						panic(err)
					}
					samlSP.ServiceProvider.IDPMetadata = idpMetadata
				}

				time.Sleep(time.Minute * 15)
			}
		}()
	}

	rootURL, err := url.Parse(fmt.Sprintf("https://%s", config.BaseDomain))
	if err != nil {
		log.Println("Error parsing root URL from configuration", err.Error())
		panic(err)
	}

	samlSP, err = samlsp.New(samlsp.Options{
		EntityID:          fmt.Sprintf("https://%s/saml/metadata", rootURL.Host),
		URL:               *rootURL,
		Key:               keyPair.PrivateKey.(*rsa.PrivateKey),
		Certificate:       keyPair.Leaf,
		IDPMetadata:       idpMetadata,
		AllowIDPInitiated: true,
		CookieSameSite:    http.SameSiteNoneMode,
	})
	if err != nil {
		log.Printf("CRITICAL : Error creating SAML SP: %s", err.Error())
		panic(err)
	}

	authServer := http.NewServeMux()
	authServer.HandleFunc("/saml/metadata.xml", samlMetadata)
	authServer.HandleFunc("/saml/metadata", samlMetadata)
	authServer.HandleFunc("/saml/slo", samlLogout)
	authServer.Handle("/saml/", samlSP)

	authServer.HandleFunc("/sso-redirect", samlRedirect)
	authServer.HandleFunc("/pass-session", samlPassSession)

	staticFiles := http.FileServer(http.FS(files))
	authServer.Handle("/assets/", staticFiles)
	authServer.HandleFunc("/style/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Serving static file: ", r.URL.Path)
		// if .woff is requested, add CORS header
		if len(r.URL.Path) > 5 && r.URL.Path[len(r.URL.Path)-5:] == ".woff" || len(r.URL.Path) > 6 && r.URL.Path[len(r.URL.Path)-6:] == ".woff2" {
			w.Header().Set("Access-Control-Allow-Origin", "*")
		}

		staticFiles.ServeHTTP(w, r)
	})

	// Handle remote API calls to the auth server
	// Allows to check if a user is logged in and to get the user's metadata
	if config.AuthServer.APIEndpoint {
		authServer.HandleFunc("/ajax/", ajaxHandler)
	}

	authServer.HandleFunc("/", mainPage)

	srv := &http.Server{
		Handler:      authServer,
		Addr:         ":8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	err = srv.ListenAndServe()
	if err != nil {
		panic(err) // TODO handle error
	}
}

func samlRedirect(w http.ResponseWriter, r *http.Request) {
	log.Println("REDIRECT TO SAML")
	// check if we are coming back from auth request
	currentSession, err := samlSP.Session.GetSession(r)

	// if there does not exist a session, redirect to SAML authentification flow
	if err == samlsp.ErrNoSession {
		samlSP.HandleStartAuthFlow(w, r)
		return
	}

	// if another error occurred, return error
	if err != nil {
		log.Println("Error getting session: ", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	r = r.WithContext(samlsp.ContextWithSession(r.Context(), currentSession))

	var dataBlock struct {
		UPN            string   `json:"UPN"`
		FullName       string   `json:"fullname"`
		SAMAccountName string   `json:"samaccountname"`
		DN             string   `json:"dn"`
		Groups         []string `json:"groups,omitempty"`
	}
	dataBlock.DN = samlsp.AttributeFromContext(r.Context(), "DN")
	if dataBlock.DN == "" {
		dataBlock.DN = samlsp.AttributeFromContext(r.Context(), "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/distinguishedname")
		if dataBlock.DN == "" {
			dataBlock.DN = samlsp.AttributeFromContext(r.Context(), "http://schemas.xmlsoap.org/claims/DistinguishedName")
			if dataBlock.DN == "" {
				log.Printf("[ERROR] :: Can't find DN in SAML response")
			}
		}
	}

	dataBlock.UPN = samlsp.AttributeFromContext(r.Context(), "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn")
	if dataBlock.UPN == "" {
		dataBlock.UPN = samlsp.AttributeFromContext(r.Context(), "http://schemas.xmlsoap.org/claims/UPN")
		if dataBlock.UPN == "" {
			log.Printf("[ERROR] :: Can't find UPN in SAML response")
		}
	}

	dataBlock.FullName = samlsp.AttributeFromContext(r.Context(), "http://schemas.xmlsoap.org/claims/CommonName")
	if dataBlock.FullName == "" {
		dataBlock.FullName = samlsp.AttributeFromContext(r.Context(), "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name")
		if dataBlock.FullName == "" {
			log.Printf("[ERROR] :: Can't find CommonName in SAML response")
		}
	}

	dataBlock.SAMAccountName = samlsp.AttributeFromContext(r.Context(), "samaccountname")
	if dataBlock.SAMAccountName == "" {
		log.Printf("[ERROR] :: Can't find samaccountname in SAML response")
	}

	sa, ok := currentSession.(samlsp.SessionWithAttributes)
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		httpWriteErrorPage(w, errorPageInformation{
			Title:   definedErrorTitleInternalError,
			Message: fmt.Sprintf(definedErrorMessageInternalError, "Can't determine session from currentSession context"),
		})
		return
	}
	attrib := sa.GetAttributes()
	if elem, ok := attrib["http://schemas.xmlsoap.org/claims/Group"]; ok {
		dataBlock.Groups = elem
	}

	// We are allowed to use SAML to create users
	// use submitted data to create a user or refresh the stored data of a user
	if config.SAML.UserCreation && !users.ExistsInDatabase(dataBlock.UPN) {
		stmtIns, err := db.Prepare("INSERT INTO `users` (`userPrincipalName`, `distinguishedName`, `sAMAccountName`, `name`) VALUES (?, ?, ?, ?) " +
			"ON DUPLICATE KEY UPDATE `userPrincipalName` = VALUES(`userPrincipalName`), `distinguishedName` = VALUES(`distinguishedName`), `sAMAccountName` = VALUES(`sAMAccountName`), `name` = VALUES(`name`)")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			httpWriteErrorPage(w, errorPageInformation{
				Title:   definedErrorTitleInternalError,
				Message: fmt.Sprintf(definedErrorMessageInternalError, err.Error()),
			})
			log.Println(err.Error())
			return
		}
		defer stmtIns.Close()

		_, err = stmtIns.Exec(dataBlock.UPN, dataBlock.DN, dataBlock.SAMAccountName, dataBlock.FullName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			httpWriteErrorPage(w, errorPageInformation{
				Title:   definedErrorTitleInternalError,
				Message: fmt.Sprintf(definedErrorMessageInternalError, err.Error()),
			})
			log.Println(err.Error())
			return
		}
	}

	// Get user
	var userID uint64
	err = db.QueryRow("SELECT IFNULL(`id`, 0) FROM `users` WHERE `userPrincipalName` = ? LIMIT 1", dataBlock.UPN).Scan(&userID)
	if err != nil {
		log.Println("MySQL :: can't fetch user from database", err.Error())
	}

	if userID == 0 {
		log.Println("user has no permissions to create session")
		w.WriteHeader(http.StatusUnauthorized)
		httpWriteErrorPage(w, errorPageInformation{
			Title:   definedErrorTitleUnauthorized,
			Message: definedErrorMessageUnauthorized,
		})
		return
	}

	// create or update user entry in local cache
	users.Add(userID, structUser{
		UserID:         userID,
		Name:           dataBlock.FullName,
		UPN:            dataBlock.UPN,
		DN:             dataBlock.DN,
		SAMAccountName: dataBlock.SAMAccountName,
		LastRefresh:    time.Now(),
	})

	// Prepare insert statement to add user group mappings
	// this links only pre-existing groups to the user to avoid creating new groups which are not intended to be used by the application
	stmtIns, err := db.Prepare("INSERT IGNORE INTO `user_groups` (`userID`, `groupID`) VALUES (?, ?)")
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer stmtIns.Close()

	// get all groups from database of the current user
	rows, err := db.Query("SELECT `groups`.`name`, `user_groups`.`groupID` FROM `user_groups` "+
		"LEFT JOIN `groups` ON `groups`.`id`=`user_groups`.`groupID` "+
		"WHERE `user_groups`.`userID` = ?", userID)
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	// check if currently saved groups are still valid for the user
	for rows.Next() {
		var groupName string
		var groupID int64
		err = rows.Scan(&groupName, &groupID)
		if err != nil {
			log.Println(err.Error())
			return
		}

		// check if the user is member of the group
		var foundInGroup bool
		for _, singleGroup := range dataBlock.Groups {
			if singleGroup == groupName {
				foundInGroup = true
				break
			}
		}

		// user not found in group, delete mapping
		if !foundInGroup {
			_, err = db.Exec("DELETE FROM `user_groups` WHERE `userID` = ? AND `groupID` = ?", userID, groupID)
			if err != nil {
				log.Println(err.Error())
				return
			}
		}
	}

	// add new groups to database with user mapping
	for _, singleGroup := range dataBlock.Groups {
		var groupID uint64
		err = db.QueryRow("SELECT IFNULL(`id`, 0) FROM `groups` WHERE `name` = ? LIMIT 1", singleGroup).Scan(&groupID)
		if err != nil {
			continue
		}

		// group exists in database, create mapping
		if groupID != 0 {
			_, err = stmtIns.Exec(userID, groupID)
			if err != nil {
				log.Println(err.Error())
				return
			}
		}

		// add group to local cache
		err = users.AddGroup(userID, groupID)
		if err != nil {
			log.Println(err.Error())
			return
		}
	}

	// Fetch group mappings from LDAP
	// This provides access to nested groups and is required for group based access control
	go func(userID uint64) {
		// if refresh is older than 1 hour, refresh it
		lastRefresh := users.GetLastRefresh(userID)
		if lastRefresh.Add(time.Hour).After(time.Now()) {
			if config.Debug {
				log.Printf("LDAP :: performing group refresh for user %d\n", userID)
			}
			err := users.RefreshUserGroupsUsingLDAP(userID)
			if err != nil {
				log.Println(err.Error())
			}
		}
	}(userID)

	// Delete old session if we overwrite one with a new one
	if oldSession, err := r.Cookie(sessionCookieName); err == nil {
		_ = session.DeleteSession(oldSession.Value)
	}

	// Create new session
	cookie := session.CreateSession(structUserSessionDetails{
		Valid:  true,
		UserID: userID,
	})

	// set session cookie
	http.SetCookie(w, &http.Cookie{
		Name:     sessionCookieName,
		Value:    cookie,
		Expires:  time.Now().Add(time.Hour * 24 * 7 * 8),
		Secure:   true,
		HttpOnly: true,
		Path:     "/",
		SameSite: http.SameSiteNoneMode,
	})

	// Delete token cookie if it exists, we do not want to use the native SAML implemented token
	// where we carry all user data within the token
	if _, err := r.Cookie("token"); err != nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "token",
			Value:    "",
			Path:     "/",
			MaxAge:   -1,
			HttpOnly: true,
			Domain:   "." + r.Host,
		})
	}

	// check if source address is given where we should redirect to
	keys, ok := r.URL.Query()["source"]
	if ok && len(keys) == 1 {
		// unescape source address
		realURL, err := url.QueryUnescape(keys[0])
		if err != nil {
			log.Println("Error unescaping source url ", err.Error(), keys[0])
			// show error page
			w.WriteHeader(http.StatusInternalServerError)
			httpWriteErrorPage(w, errorPageInformation{
				Title:   definedErrorTitleInternalError,
				Message: fmt.Sprintf(definedErrorMessageInternalError, template.HTMLEscapeString(err.Error())),
			})
			return
		}

		// parse URL to extract Host to be checked if it is known and a valid redirect target
		parsedURL, err := url.Parse(realURL)
		if err != nil {
			log.Println("Error parsing source url :: ", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			httpWriteErrorPage(w, errorPageInformation{
				Title:   definedErrorTitleInternalError,
				Message: fmt.Sprintf(definedErrorMessageInternalError, template.HTMLEscapeString(err.Error())),
			})
			return
		}

		// If domain is in allowed list of existing domains, allow free redirect
		// to the source to set the session for the current domain
		if cacheApplications.GetDomain(parsedURL.Host).Domain != "" {
			http.Redirect(w, r, fmt.Sprintf("https://%s/__passbeyond-session?cookie=%s&source=%s",
				parsedURL.Host,
				url.QueryEscape(cookie),
				url.QueryEscape(parsedURL.String()),
			), http.StatusSeeOther)
			return
		}
	}

	// no source address given, there is no page to display to the user
	// if a redirect page is configured, redirect to it
	// otherwise redirect to start page
	if config.AuthServer.StartPageRedirect != "" {
		http.Redirect(w, r, config.AuthServer.StartPageRedirect, http.StatusSeeOther)
		return
	}

	w.Header().Add("Location", "/")
}

func samlLogout(w http.ResponseWriter, r *http.Request) {

}

func mainPage(w http.ResponseWriter, r *http.Request) {
	// Get redirect target
	keys, ok := r.URL.Query()["source"]
	if ok || len(keys) == 1 {
		http.Redirect(w, r, fmt.Sprintf("/sso-redirect?source=%s", url.QueryEscape(keys[0])), http.StatusTemporaryRedirect)
		_, _ = w.Write([]byte("redirecting main to /sso-redirect"))
		// TODO: Error source missing
		return
	}

	// if cookie "token" is set, delete it as it is an artifact of an previous login
	if _, err := r.Cookie("token"); err == nil {
		http.SetCookie(w, &http.Cookie{
			Name:    "token",
			Value:   "",
			Path:    "/",
			MaxAge:  -1,
			Domain:  "." + r.Host,
			Expires: time.Now().Add(-1 * time.Hour),
		})
	}

	// if main page redirect is set, redirect instead of showing an error page
	if config.AuthServer.StartPageRedirect != "" {
		http.Redirect(w, r, config.AuthServer.StartPageRedirect, http.StatusSeeOther)
		return
	}

	w.WriteHeader(http.StatusNotFound)

	httpWriteErrorPage(w, errorPageInformation{
		Title: "PassBeyond",
		Message: `PassBeyond is a reverse proxy that validates requests for authorization and authentication before forwarding them to the destination service.<br>
<br>
There is no other content on this page. Go to a page secured by PassBeyond so that you are automatically logged in through this service.<br>
<br>
<strong>How does PassBeyond work?</strong><br>
Before a request reaches the target service, PassBeyond checks whether the user is authorized to access the service. In this way, services offered on the web are effectively protected against unauthorized access.<br>
Any number of services can be secured by a centrally provided instance. SAML-based login and Active Directory as user backend enable seamless single sign-on for an excellent user experience.<br>
<br>
<strong>Want to learn more or use it for your business, university or personal use?</strong><br>
PassBeyond is <a href="https://bella.network/passbeyond?utm_source=passbeyond">free and open source software</a>.`,
	})
}

func samlPassSession(w http.ResponseWriter, r *http.Request) {
	// Get redirect target
	keys, ok := r.URL.Query()["source"]
	if !ok || len(keys[0]) < 1 {
		// show error page that no source is given
		httpWriteErrorPage(w, errorPageInformation{
			Title: definedErrorTitleInternalError,
			Message: fmt.Sprintf(definedErrorMessageInternalError,
				"The parameter source is missing, which causes the redirection back to the original page to fail.\n"+
					"Please contact the administrator of this service for more information.",
			),
		})
		return
	}

	unescapedURL, err := url.QueryUnescape(keys[0])
	if err != nil {
		log.Println("error unescaping url", err.Error(), keys[0])
		// show error page that no source is given
		httpWriteErrorPage(w, errorPageInformation{
			Title: definedErrorTitleInternalError,
			Message: fmt.Sprintf(definedErrorMessageInternalError,
				"The parameter source can't be unescaped, which causes the redirection back to the original page to fail.\n"+
					"Error details: "+err.Error(),
			),
		})
		return
	}

	parsedURL, err := url.Parse(unescapedURL)
	if err != nil {
		log.Println("error parsing url", err.Error(), parsedURL)
		// show error page that parsing the source url failed
		httpWriteErrorPage(w, errorPageInformation{
			Title: definedErrorTitleInternalError,
			Message: fmt.Sprintf(definedErrorMessageInternalError,
				"The parameter source can't be parsed, which causes the redirection back to the original page to fail.\n"+
					"Error details: "+err.Error(),
			),
		})
		return
	}

	thisSessionID, err := session.CheckValidHTTPSession(r)
	if err != nil || thisSessionID == 0 {
		http.Redirect(w, r, "/?source="+url.QueryEscape(parsedURL.String()), 307)
		//nolint:lll
		if _, err := w.Write([]byte("This user session is not valid. Redirecting you to <a href=\"/?source=" + url.QueryEscape(parsedURL.String()) + "\">Login</a> for reauthentication")); err != nil {
			log.Println("error writing response", err.Error())
		}
		return
	}

	// Check if target is valid
	app := cacheApplications.GetDomain(parsedURL.Host)

	// Target was not found
	if app.Domain == "" {
		w.WriteHeader(http.StatusNotFound)
		httpWriteErrorPage(w, errorPageInformation{
			Title:   definedErrorTitleDomainUnknown,
			Message: definedErrorMessageDomainUnknown,
		})
		return
	}

	sessionCookie, _ := r.Cookie(sessionCookieName)
	// Use 303 redirect here to prevent forward of POST or PUT data
	// Client should do a GET request to prevent possible leak of user data

	http.Redirect(w, r, fmt.Sprintf("https://%s/__passbeyond-session?cookie=%s&source=%s",
		app.Domain,
		url.QueryEscape(sessionCookie.Value),
		url.QueryEscape(parsedURL.String()),
	), http.StatusSeeOther)
}

// samlDownloadMetadataFile is used to download the metadata file for the SAML service provider
// returns true if the file is different to the previous one
func samlDownloadMetadataFile(metadataURL string) (bool, error) {
	// Read local metadata file
	// Ignore error if file does not exist, in this case overwrite is forced
	localMetadataFile, _ := os.ReadFile(idpMetadataPath)

	// Fetch remote metadata file
	client := http.Client{
		Timeout: 30 * time.Second,
	}
	resp, err := client.Get(metadataURL)
	if err != nil {
		log.Println("Error downloading metadata file ", err.Error())
		return false, err
	}
	defer resp.Body.Close()

	// Read remote metadata file into variable
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading metadata file ", err.Error())
		return false, err
	}

	// compare if local version is different to remote version
	if bytes.Equal(localMetadataFile, body) {
		return false, nil
	}

	// Metadata file is different, write new file and return true for further processing
	err = os.WriteFile(idpMetadataPath, body, 0644) //nolint:gosec
	if err != nil {
		log.Println("Error saving metadata file ", err.Error())
		return false, err
	}

	return true, nil
}

// samlMetadata returns the metadata file for the SAML service provider
func samlMetadata(w http.ResponseWriter, r *http.Request) {
	metadata := samlMetadataManipulate()

	// if metadata.xml was requested, return text/xml content type
	if strings.HasSuffix(r.URL.Path, "metadata.xml") {
		w.Header().Set("Content-Type", "text/xml")
	} else {
		w.Header().Set("Content-Type", "application/samlmetadata+xml")
	}

	buf, _ := xml.MarshalIndent(metadata, "", "  ")
	_, _ = w.Write(buf)
}

func samlMetadataManipulate() *saml.EntityDescriptor {
	samlED := samlSP.ServiceProvider.Metadata()

	// Set NameIDFormat to transient
	// as of crewjam/saml v0.4.10 NameIDFormat is not set and causes an error in ADFS
	samlED.SPSSODescriptors[0].SSODescriptor.NameIDFormats = []saml.NameIDFormat{
		saml.NameIDFormat("urn:oasis:names:tc:SAML:2.0:nameid-format:transient"),
	}

	/*
		Request attributes from IdP
		NOTE: This is not supported by ADFS.
		If any IdP does support this functionality, the following code can be used to request attributes.
	*/
	var samlRequestAttributeStore []saml.AttributeConsumingService
	req := true

	samlRequestAttributeStore = append(samlRequestAttributeStore, saml.AttributeConsumingService{
		Index: 1,
		ServiceNames: []saml.LocalizedName{
			{
				Lang:  "en",
				Value: "Basic attributes for PassBeyond",
			},
		},
		RequestedAttributes: []saml.RequestedAttribute{
			{
				Attribute: saml.Attribute{
					NameFormat:   "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
					Name:         "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn",
					FriendlyName: "UPN",
				},
				IsRequired: &req,
			},
			{
				Attribute: saml.Attribute{
					NameFormat:   "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
					Name:         "urn:oid:2.16.840.1.113730.3.1.241",
					FriendlyName: "displayName",
				},
			},
		},
	})

	samlED.SPSSODescriptors[0].AttributeConsumingServices = samlRequestAttributeStore

	return samlED
}
