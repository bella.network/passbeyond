package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// db contains the global MySQL database connection
var db *sql.DB

// databaseSchemaVersion contains the current database schema version
const databaseSchemaVersion = 1

// initiateMySQL will initiate the connection to the database
// and periodically checks if the connection is still alive
func initiateMySQL() {
	connectMySQL()

	go func() {
		for {
			time.Sleep(time.Minute * 5)
			checkMySQLconnection()
		}
	}()
}

// connectMySQL initiates a connection to the predefined database
func connectMySQL() {
	var err error
	log.Println("Opening mysql connection")
	db, err = sql.Open("mysql",
		fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4,utf8&timeout=5s&readTimeout=15s&writeTimeout=15s&parseTime=true",
			config.Database.Username,
			config.Database.Password,
			config.Database.Server,
			config.Database.Database,
		))
	if err != nil {
		log.Println("Failed to connect to MySQL database, retry in 1 minute", err)
		destructMySQL()
		go func() {
			time.Sleep(time.Second * 60)
			connectMySQL()
		}()
		return
	}
	log.Println("Setting mysql settings")
	// show global variables like 'wait_timeout';
	db.SetConnMaxLifetime(time.Second * 600 / 2)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(5)

	// actively connect to the database
	err = db.Ping()
	if err != nil {
		log.Println("Failed to connect to MySQL database, retry in 1 minute", err)
		destructMySQL()
		go func() {
			time.Sleep(time.Second * 60)
			connectMySQL()
		}()
		return
	}
}

// initiates the database schema if it does not exist
// returns true if the schema exists or was created
// creates schema in version 1
func initiateMySQLSchema() bool {
	log.Println("Initiating MySQL schema")

	// check if table meta exists
	var tableExists int
	err := db.QueryRow("SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = ? AND table_name = 'meta'", config.Database.Database).Scan(&tableExists)
	if err != nil {
		log.Println("Failed to check if table meta exists", err)
		return false
	}

	// database schema does not exist
	// import schema from database.sql file
	if tableExists == 0 {
		_, err = db.Exec("CREATE TABLE `applications` (" +
			"`id` int(10) unsigned NOT NULL AUTO_INCREMENT," +
			"`name` varchar(100) DEFAULT NULL," +
			"`domain` varchar(100) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL," +
			"`destination` varchar(100) NOT NULL," +
			"`description` text NOT NULL DEFAULT ''," +
			"`trustedNetworkPasstrough` tinyint(1) unsigned NOT NULL DEFAULT 0," +
			"`skipTLSVerification` tinyint(1) unsigned NOT NULL DEFAULT 0," +
			"`basicAuthorization` tinyint(1) unsigned NOT NULL DEFAULT 0," +
			"PRIMARY KEY (`id`)," +
			"UNIQUE KEY `applications_domain` (`domain`)" +
			") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table applications", err)
			return false
		}

		_, err = db.Exec("CREATE TABLE `groups` (" +
			"`id` int(10) unsigned NOT NULL AUTO_INCREMENT," +
			"`name` varchar(100) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL," +
			"`DN` varchar(500) NOT NULL DEFAULT ''," +
			"PRIMARY KEY (`id`)," +
			"UNIQUE KEY `groups_name` (`name`)" +
			") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table groups", err)
			return false
		}

		_, err := db.Exec("CREATE TABLE `application_groups` (" +
			"`applicationID` int(10) unsigned NOT NULL," +
			"`groupID` int(10) unsigned NOT NULL," +
			"UNIQUE KEY `application_groups_UNIQUE` (`applicationID`,`groupID`)," +
			"KEY `application_groups_FK_1` (`groupID`)," +
			"CONSTRAINT `application_groups_FK` FOREIGN KEY (`applicationID`) REFERENCES `applications` (`id`) ON DELETE CASCADE," +
			"CONSTRAINT `application_groups_FK_1` FOREIGN KEY (`groupID`) REFERENCES `groups` (`id`) ON DELETE CASCADE" +
			") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table application_groups", err)
			return false
		}

		_, err = db.Exec("CREATE TABLE `meta` (" +
			"`key` varchar(100) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL," +
			"`value` text NOT NULL," +
			"PRIMARY KEY (`key`)" +
			") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table meta", err)
			return false
		}

		_, err = db.Exec("INSERT INTO `meta` (`key`, `value`) VALUES ('schemaVersion', '1')")
		if err != nil {
			log.Println("Failed to insert schema version", err)
			return false
		}

		_, err = db.Exec("CREATE TABLE `users` (" +
			"`id` int(10) unsigned NOT NULL AUTO_INCREMENT," +
			"`userPrincipalName` varchar(255) NOT NULL," +
			"`distinguishedName` varchar(500) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL," +
			"`sAMAccountName` varchar(500) DEFAULT NULL," +
			"`name` varchar(255) NOT NULL," +
			"`creation` timestamp NOT NULL DEFAULT current_timestamp()," +
			"`refresh` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()," +
			"PRIMARY KEY (`id`)," +
			"UNIQUE KEY `users_UPN` (`userPrincipalName`)," +
			"UNIQUE KEY `users_DN` (`distinguishedName`)," +
			"UNIQUE KEY `users_SAM` (`sAMAccountName`)" +
			") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table users", err)
			return false
		}

		_, err = db.Exec("CREATE TABLE `user_groups` (" +
			"`userID` int(10) unsigned NOT NULL," +
			"`groupID` int(10) unsigned NOT NULL," +
			"`update` datetime NOT NULL DEFAULT current_timestamp()," +
			"UNIQUE KEY `user_groups_UNIQUE` (`userID`,`groupID`)," +
			"KEY `user_groups_FK_1` (`groupID`)," +
			"CONSTRAINT `user_groups_FK` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE," +
			"CONSTRAINT `user_groups_FK_1` FOREIGN KEY (`groupID`) REFERENCES `groups` (`id`) ON DELETE CASCADE" +
			") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table user_groups", err)
			return false
		}

		_, err = db.Exec("CREATE TABLE `sessions` (" +
			"`id` char(40) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL," +
			"`userID` int(10) unsigned NOT NULL," +
			"`created` timestamp NOT NULL DEFAULT current_timestamp()," +
			"`last_used` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()," +
			"PRIMARY KEY (`id`)," +
			"KEY `sessions_FK` (`userID`)," +
			"CONSTRAINT `sessions_FK` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE" +
			") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;")
		if err != nil {
			log.Println("Failed to create table sessions", err)
			return false
		}
	}

	return true
}

// check if schema update is required
func checkSchemaUpdate() {
	// ignore error message as on fail we assume a schema version of 0
	var currentVersion int
	_ = db.QueryRow("SELECT `value` FROM `meta` WHERE `key` = 'schemaVersion'").Scan(&currentVersion)

	if currentVersion < databaseSchemaVersion {
		log.Println("Schema update required")
		updateDatabaseSchema(currentVersion)
	}
}

// updateDatabaseSchema updates the database schema to the latest version
func updateDatabaseSchema(currentVersion int) {
	if currentVersion < 1 {
		// Version 1 is the minimum required version
		// versions below 1 are not supported as they are not defined or development versions
		panic("Database schema version is below 1. This is not supported")
	}
}

// destructMySQL closes the connection to the database
func destructMySQL() {
	log.Println("Closing connection")
	db.Close()
}

// checkMySQLconnection sends a ping to the database to check if it is reachable
// If not, it calls destructMySQL() and connectMySQL() to establish a new connection
func checkMySQLconnection() {
	c1 := make(chan string, 1)

	go func() {
		err := db.Ping()
		if err != nil {
			log.Println("Connection to database lost. Reconnecting", err)
			destructMySQL()
			connectMySQL()
		}
		c1 <- "ping"
	}()

	select {
	case <-c1:
		// nothing to do
	case <-time.After(time.Second * 15):
		log.Println("Timeout on db.Ping - killing connection and reconnecting")
		destructMySQL()
		connectMySQL()
	}
}
