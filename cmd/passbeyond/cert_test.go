package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"os"
	"testing"
	"time"
)

// TestGeneratePublicAndPrivateKey tests the generation of a public and private key
func TestGeneratePublicAndPrivateKey(t *testing.T) {
	cert, err := generatePublicAndPrivateKey("example.com")
	if err != nil {
		t.Error(err)
	}

	if cert.PrivateKey == nil {
		t.Error("Private key is nil")
	}

	if cert.Certificate == nil {
		t.Error("Certificate is nil")
	}

	if len(cert.Certificate) == 0 {
		t.Error("Certificate is empty")
	}

	if cert.Certificate == nil {
		t.Error("Leaf public key is nil")
	}

	if cert.Leaf.PublicKeyAlgorithm != 1 {
		t.Error("Leaf public key algorithm is not RSA")
	}

	if cert.Leaf.PublicKey.(*rsa.PublicKey).N == nil {
		t.Error("Leaf public key N is nil")
	}

	if cert.Leaf.PublicKey.(*rsa.PublicKey).E == 0 {
		t.Error("Leaf public key E is 0")
	}

	if cert.Leaf.PublicKey.(*rsa.PublicKey).Size() == 0 {
		t.Error("Leaf public key size is 0")
	}

	// check if public key matches private key
	if cert.Leaf.PublicKey.(*rsa.PublicKey).N.Cmp(cert.PrivateKey.(*rsa.PrivateKey).N) != 0 {
		t.Error("Public key does not match private key")
	}
}

func TestCertCheckIfExpired(t *testing.T) {
	cert, err := generatePublicAndPrivateKey("example.com")
	if err != nil {
		t.Error(err)
	}

	if certCheckIfExpired(cert) {
		t.Error("Certificate is expired which should not be the case as it was just generated")
	}
}

func TestCertCheckIfExpiredWithExpiredCert(t *testing.T) {
	// generate expired certificate
	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"PassBeyond"},
			CommonName:   "PassBeyond",
		},
		NotBefore: time.Now().Add(time.Hour * 24 * 100 * -1),
		NotAfter:  time.Now().Add(time.Hour * 24 * 5 * -1),
		KeyUsage:  x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageServerAuth,
		},
		DNSNames: []string{
			"example.com",
		},
	}
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Error(err)
	}
	pub := &priv.PublicKey
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pub, priv)
	if err != nil {
		t.Error(err)
	}

	// write x509 certificate into tls.certificate
	publicKey := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	privateKey := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})

	cert, err := tls.X509KeyPair(publicKey, privateKey)
	if err != nil {
		t.Error(err)
	}

	if !certCheckIfExpired(&cert) {
		t.Error("Certificate is not expired which should be the case as it was generated in the past")
	}
}

// nolint
func TestSamlLoadKeyPair(t *testing.T) {
	// no certificate given, autogeneration should be used
	cert, err := samlLoadKeyPair(samlKeySettings{
		FallbackPublicKeyPath:  "fpkp.crt",
		FallbackPrivateKeyPath: "fpkp.key",
		FallbackDomain:         "example.com",
	})
	if err != nil {
		t.Error(err)
	}

	if cert.PrivateKey == nil {
		t.Error("Private key is nil")
	}

	if cert.Certificate == nil {
		t.Error("Certificate is nil")
	}

	// check if files were created
	if _, err := os.Stat("fpkp.crt"); os.IsNotExist(err) {
		t.Error("Public key file was not created")
	}

	if _, err := os.Stat("fpkp.key"); os.IsNotExist(err) {
		t.Error("Private key file was not created")
	}

	// everything worked, files exist and load them correctly
	cert, err = samlLoadKeyPair(samlKeySettings{
		PublicKeyPath:          "fpkp.crt",
		PrivateKeyPath:         "fpkp.key",
		FallbackPublicKeyPath:  "fpkp2.crt",
		FallbackPrivateKeyPath: "fpkp2.key",
		FallbackDomain:         "example.com",
	})
	if err != nil {
		t.Error(err)
	}

	if cert.PrivateKey == nil {
		t.Error("Private key is nil")
	}

	if cert.Certificate == nil {
		t.Error("Certificate is nil")
	}

	// fallback files should not exist
	if _, err := os.Stat("fpkp2.crt"); !os.IsNotExist(err) {
		os.Remove("fpkp2.crt")
		os.Remove("fpkp2.key")
		t.Error("Public key file was created")
	}

	if _, err := os.Stat("fpkp2.key"); !os.IsNotExist(err) {
		os.Remove("fpkp2.crt")
		os.Remove("fpkp2.key")
		t.Error("Private key file was created")
	}

	// tests finished, remove files
	os.Remove("fpkp.crt")
	os.Remove("fpkp.key")
}
