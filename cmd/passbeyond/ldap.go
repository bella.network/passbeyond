package main

import (
	"errors"
	"fmt"
	"log"
	"net"
	"strconv"
	"time"

	"github.com/go-ldap/ldap/v3"
)

// ldapConnection is the LDAP connection to the configured LDAP server
var ldapConnection *ldap.Conn

func initiateLDAP() error {
	if err := ldapConnect(); err != nil {
		return err
	}
	if config.Debug {
		log.Println("LDAP connection established")
	}

	// reconnect to LDAP server if connection is lost
	go func() {
		time.Sleep(time.Second * 5)
		for {
			// check if connection is nil
			if ldapConnection == nil {
				if config.Debug {
					log.Println("LDAP connection lost, reconnecting")
				}

				if err := ldapConnect(); err != nil {
					log.Println("failed to reconnect to LDAP server:", err)
				} else if config.Debug {
					log.Println("LDAP connection established")
				}
			}

			// check if connection is closing
			if ldapConnection.IsClosing() {
				if config.Debug {
					log.Println("LDAP connection lost, reconnecting")
				}

				if err := ldapConnect(); err != nil {
					log.Println("failed to reconnect to LDAP server:", err)
				} else if config.Debug {
					log.Println("LDAP connection established")
				}
			}

			time.Sleep(5 * time.Second)
		}
	}()

	return nil
}

func ldapConnect() error {
	// connect to LDAP server
	protocol := "ldap"
	port := config.LDAP.Port
	if config.LDAP.LDAPS {
		protocol = "ldaps"
		// if port is not set, use LDAPS default port
		if port == 0 {
			port = 636
		}
	}

	// Port is not set, use default LDAP port
	if port == 0 {
		port = 389
	}

	var srvAddresses []*net.SRV
	// if AD-Site is set, use it to discover LDAP server
	if config.LDAP.ADSite != "" {
		if config.Debug {
			log.Println("Discovering LDAP server for AD-Site", config.LDAP.ADSite)
		}
		// get LDAP server from AD-Site
		// _ldap._tcp.<SiteName>._sites.<DNSDomainName>
		_, addrs, err := net.LookupSRV(
			"", "",
			fmt.Sprintf("_ldap._tcp.%s._sites.%s", config.LDAP.ADSite, config.LDAP.Server),
		)
		if err != nil {
			return fmt.Errorf("failed to get LDAP server from AD-Site: %s", err)
		}
		srvAddresses = addrs
	} else {
		// No AD Site is configured, try to look up LDAP server using SRV record
		_, addrs, err := net.LookupSRV("ldap", "tcp", config.LDAP.Server)
		if err != nil && config.Debug {
			log.Println("LDAP: failed to perform SRV lookup for LDAP server:", err)
		}
		srvAddresses = addrs
	}

	// Always fall back to the configured server address as last resort
	// this may fail if servers are not reachable - which is a hard fail as all servers are unreachable
	srvAddresses = append(srvAddresses, &net.SRV{
		Target: config.LDAP.Server,
		Port:   uint16(port),
	})

	if config.Debug {
		log.Printf("LDAP: found %d %s servers", len(srvAddresses), protocol)
		// print list of servers
		for _, addr := range srvAddresses {
			log.Printf("LDAP: server: %s:%d", addr.Target, addr.Port)
		}
	}

	// Try to connect to the LDAP server
	var err error
	for _, addr := range srvAddresses {
		// SRV discovered servers may have a trailing dot, remove it
		if addr.Target[len(addr.Target)-1] == '.' {
			addr.Target = addr.Target[:len(addr.Target)-1]
		}

		if config.Debug {
			log.Printf("LDAP: trying to connect to %s server %s:%d", protocol, addr.Target, port)
		}

		// servers discovered via SRV records are always LDAP with port 389
		// as LDAPS may be configured, we enforce the configured protocol globally
		if err = ldapDialTargetAndBind(
			fmt.Sprintf("%s://%s:%d", protocol, addr.Target, port),
			config.LDAP.BindDN,
			config.LDAP.Password,
		); err == nil {
			break
		} else if config.Debug {
			log.Println("LDAP: failed to connect to server:", err)
		}
	}

	if err != nil {
		return fmt.Errorf("failed to connect to LDAP server: %s", err)
	} else if config.Debug {
		log.Println("LDAP: connected to LDAP server")
	}

	return nil
}

// ldapDialTargetAndBind connects to the LDAP server and binds to it
func ldapDialTargetAndBind(target string, bindDN string, password string) error {
	// connect to LDAP server
	var err error
	ldapConnection, err = ldap.DialURL(target, ldap.DialWithDialer(&net.Dialer{
		Timeout:   7 * time.Second,
		KeepAlive: 5 * time.Second,
	}))
	if err != nil {
		return err
	}

	// bind to LDAP server
	err = ldapConnection.Bind(bindDN, password)
	if err != nil {
		return err
	}

	return nil
}

const ldapFilter = "(&(objectClass=%s)(%s=%s))"

var errUserNotFound = errors.New("user not found")
var errUserDisabled = errors.New("user disabled")

// ldapFetchUserByDN fetches a user from LDAP by its distinguished name
func ldapFetchUserByDN(DN string) (ldapUser, error) {
	// search for user
	searchRequest := ldap.NewSearchRequest(
		config.LDAP.BaseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(ldapFilter, "person", "distinguishedName", DN),
		[]string{
			"dn",
			"cn", "mail", "memberOf", "pwdLastSet", "thumbnailPhoto", "userPrincipalName", "sAMAccountName",
			"userAccountControl", // determine if account is disabled
		},
		nil,
	)

	// perform search
	sr, err := ldapConnection.Search(searchRequest)
	if err != nil {
		return ldapUser{}, err
	}

	if len(sr.Entries) != 1 {
		return ldapUser{}, errUserNotFound
	}

	// determine if account is disabled
	userAccountControl := sr.Entries[0].GetAttributeValue("userAccountControl")
	if userAccountControl != "" {
		uac, err := strconv.Atoi(userAccountControl)
		if err != nil {
			return ldapUser{}, fmt.Errorf("failed to convert userAccountControl to integer: %s", err)
		}

		// check for disabled account
		if uac&2 == 2 {
			return ldapUser{}, errUserDisabled
		}
	}

	// determine last password change
	var lastPWchange time.Time
	pwdLastSet := sr.Entries[0].GetAttributeValue("pwdLastSet")
	if pwdLastSet != "" {
		pwdLastSetTime, err := ldapTimestampToTime(pwdLastSet)
		if err == nil {
			lastPWchange = pwdLastSetTime
		}
	}

	return ldapUser{
		DN:                sr.Entries[0].DN,
		CN:                sr.Entries[0].GetAttributeValue("cn"),
		SAMAccountName:    sr.Entries[0].GetAttributeValue("sAMAccountName"),
		Mail:              sr.Entries[0].GetAttributeValue("mail"),
		MemberOf:          sr.Entries[0].GetAttributeValues("memberOf"),
		PwdLastSet:        lastPWchange,
		ThumbnailPhoto:    sr.Entries[0].GetRawAttributeValue("thumbnailPhoto"),
		UserPrincipalName: sr.Entries[0].GetAttributeValue("userPrincipalName"),
	}, nil
}

// ldapFetchAllUsersGroupsByDN fetches all groups a user is member of by its distinguished name
func ldapFetchAllUsersGroupsByDN(DN string) ([]string, error) {
	// search for user
	searchRequest := ldap.NewSearchRequest(
		config.LDAP.BaseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(
			"(&(objectClass=%s)(member:1.2.840.113556.1.4.1941:=%s))",
			"group",
			DN,
		),
		[]string{
			"cn",
			"dn",
		},
		nil,
	)

	// perform search
	sr, err := ldapConnection.Search(searchRequest)
	if err != nil {
		return []string{}, err
	}

	groups := []string{}
	for _, entry := range sr.Entries {
		groups = append(groups, entry.GetAttributeValue("cn"))
	}

	return groups, nil
}

// ldapFetchGroupInfoByDN fetches a group from LDAP by its distinguished name
func ldapFetchGroupInfoByDN(groupDN string) (ldapGroup, error) {
	// search for group
	searchRequest := ldap.NewSearchRequest(
		config.LDAP.BaseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(ldapFilter, "group", "distinguishedName", groupDN),
		[]string{
			"dn",
			"cn", "description", "mail", "member", "objectGUID",
		},
		nil,
	)

	// perform search
	sr, err := ldapConnection.Search(searchRequest)
	if err != nil {
		return ldapGroup{}, err
	}

	if len(sr.Entries) != 1 {
		return ldapGroup{}, fmt.Errorf("group does not exist or too many entries returned")
	}

	return ldapGroup{
		DN:      sr.Entries[0].DN,
		CN:      sr.Entries[0].GetAttributeValue("cn"),
		Members: sr.Entries[0].GetAttributeValues("member"),
	}, nil
}

// ldapCheckUserEnabled checks if the user is enabled in LDAP
func ldapCheckUserEnabled(UPN string) (bool, error) {
	// search for user
	searchRequest := ldap.NewSearchRequest(
		config.LDAP.BaseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(ldapFilter, "person", "userPrincipalName", UPN),
		[]string{
			"dn",
			"userAccountControl", // determine if account is disabled
		},
		nil,
	)

	// perform search
	sr, err := ldapConnection.Search(searchRequest)
	if err != nil {
		return false, err
	}

	if len(sr.Entries) != 1 {
		return false, fmt.Errorf("User does not exist or too many entries returned")
	}

	// determine if account is disabled
	userAccountControl := sr.Entries[0].GetAttributeValue("userAccountControl")
	if userAccountControl != "" {
		uac, err := strconv.Atoi(userAccountControl)
		if err != nil {
			return false, fmt.Errorf("Failed to convert userAccountControl to integer: %s", err)
		}

		// check for disabled account
		if uac&2 == 2 {
			return false, nil
		}
	}

	return true, nil
}

/*
// serve binary jpeg data as image using http
func serveJPEG(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/jpeg")
	w.Write([]byte(jpegData))
}
*/

type ldapUser struct {
	Enabled           bool      `json:"enabled"`
	DN                string    `json:"dn"`
	CN                string    `json:"cn"`
	SAMAccountName    string    `json:"sAMAccountName"`
	Mail              string    `json:"mail"`
	MemberOf          []string  `json:"memberOf"`
	PwdLastSet        time.Time `json:"pwdLastSet"`
	ThumbnailPhoto    []byte    `json:"thumbnailPhoto"`
	UserPrincipalName string    `json:"UPN"`
}

type ldapGroup struct {
	DN      string   `json:"dn"`
	CN      string   `json:"cn"`
	Members []string `json:"members"`
}

// ldapTimestampToTime converts a timestamp from LDAP to a time.Time object
func ldapTimestampToTime(timestamp string) (time.Time, error) {
	// convert timestamp to int64
	timestampInt64, err := strconv.ParseInt(timestamp, 10, 64)
	if err != nil {
		return time.Time{}, fmt.Errorf("failed to convert timestamp to int64: %s", err)
	}

	// convert timestamp to time
	timestampTime := time.Unix(timestampInt64/10000000-11644473600, 0)

	return timestampTime, nil
}
