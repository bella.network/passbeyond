package main

import (
	"embed"
	"html/template"
	"io/fs"
)

var (
	//go:embed theme assets style
	files     embed.FS
	templates map[string]*template.Template
)

// loadTemplates loads all templates from the theme directory
func loadTemplates() error {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	tmplFiles, err := fs.ReadDir(files, "theme")
	if err != nil {
		return err
	}

	// Parse all embedded templates of embed.FS bundled files
	for _, tmpl := range tmplFiles {
		if tmpl.IsDir() {
			continue
		}

		// parse template
		parsedTheme, err := template.ParseFS(files, "theme"+"/"+tmpl.Name())
		if err != nil {
			return err
		}

		// Add template to map of templates
		templates[tmpl.Name()] = parsedTheme
	}
	return nil
}
