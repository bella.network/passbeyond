package main

import (
	"crypto/rand"
	"errors"
	"log"
	"net/http"
	"sync"
	"time"
)

var session structUserSession
var sessionLastUsed structSessionLastUsed

type structUserSession struct {
	Sessions map[string]structUserSessionDetails `json:"sessions"`
	rw       sync.RWMutex
}

type structUserSessionDetails struct {
	Valid        bool      `json:"valid"`
	UserID       uint64    `json:"userID"`
	CreationTime time.Time `json:"creationTime"`
	RefreshTime  time.Time `json:"refreshTime"`
}

type structSessionLastUsed struct {
	Sessions map[string]time.Time `json:"sessions"`
	rw       sync.RWMutex
}

// CheckValidHTTPSession checks if the session is valid based on http.Request Cookie information
// and returns the users ID
func (s *structUserSession) CheckValidHTTPSession(r *http.Request) (uint64, error) {
	sessionCookie, err := r.Cookie(sessionCookieName)
	if err != nil {
		return 0, err
	}
	if sessionCookie.Value == "" {
		return 0, errors.New("Session cookie is empty")
	}

	return s.CheckValidSession(sessionCookie.Value)
}

// CheckValidSession checks if the session is valid and returns the users session details
func (s *structUserSession) CheckValidSession(sessionID string) (uint64, error) {
	var userDetails structUserSessionDetails
	// TODO: check if user is allowed to use application

	// check if data is available from local session
	s.rw.RLock()
	if data, ok := s.Sessions[sessionID]; ok {
		userDetails = data
	}
	s.rw.RUnlock()

	// not available, check from MySQL database
	if !userDetails.Valid || userDetails.UserID == 0 {
		if uid := s.GetUserIDFromDBSession(sessionID); uid > 0 {
			userDetails = structUserSessionDetails{
				Valid:        true,
				UserID:       uid,
				CreationTime: time.Now(),
				RefreshTime:  time.Now(),
			}

			if config.Debug {
				log.Printf("Adding session %s to local session storage of user %d", sessionID, uid)
			}

			// check if userID is cached in users table
			if !users.UserExists(uid) {
				err := users.AddFromExternalSource(uid)
				if err != nil {
					return 0, err
				}
				if config.Debug {
					log.Printf("Added user %d to local user storage", uid)
				}
			}

			s.rw.Lock()
			if s.Sessions == nil {
				s.Sessions = make(map[string]structUserSessionDetails)
			}
			s.Sessions[sessionID] = userDetails
			s.rw.Unlock()
		}
	}

	// check if no valid session was found
	if !userDetails.Valid || userDetails.UserID == 0 {
		return 0, errors.New("Session is not valid")
	}

	// check if session needs a refresh
	if s.checkRefresh(userDetails.RefreshTime) {
		if config.Debug {
			log.Printf("Refreshing session %s for user %d", sessionID, userDetails.UserID)
		}
		userDetails.RefreshTime = time.Now()

		// Check if session is still valid in database
		sessID := s.GetUserIDFromDBSession(sessionID)
		if sessID == 0 {
			return 0, errors.New("Session is not valid")
		}

		if !users.UserExists(userDetails.UserID) {
			err := users.AddFromExternalSource(userDetails.UserID)
			if err != nil {
				return 0, err
			}
			if config.Debug {
				log.Printf("Added user %d to local user storage", userDetails.UserID)
			}
		}

		// Check if user is enabled in LDAP
		if ok, err := ldapCheckUserEnabled(users.GetUPN(userDetails.UserID)); !ok || err != nil {
			return 0, errors.New("User is not enabled")
		}

		s.rw.Lock()
		s.Sessions[sessionID] = userDetails
		s.rw.Unlock()
	}

	return userDetails.UserID, nil
}

// CreateSession creates a new session for the user
func (s *structUserSession) CreateSession(details structUserSessionDetails) string {
	newSessionCookieID, err := s.generateRandomString(32)
	if err != nil {
		log.Println("Error generating session cookie", err.Error())
		return ""
	}

	s.rw.Lock()
	// Check if session table is not initialized yet and create it if needed
	if s.Sessions == nil {
		s.Sessions = make(map[string]structUserSessionDetails)
	}
	s.Sessions[newSessionCookieID] = details
	s.rw.Unlock()

	// async insert into database of new session
	go func(cookie string, userID uint64) {
		stmtIns, err := db.Prepare("INSERT IGNORE INTO `sessions` (`id`, `userID`) VALUES (?, ?)")
		if err != nil {
			log.Println(err.Error())
			return
		}
		defer stmtIns.Close()

		_, err = stmtIns.Exec(cookie, userID)
		if err != nil {
			log.Println(err.Error())
			return
		}
	}(newSessionCookieID, details.UserID)

	return newSessionCookieID
}

// DeleteSession deletes the session from the database and the local session
func (s *structUserSession) DeleteSession(sessionID string) error {
	// We need to wait for mysql to not allow session creation while mysql query is being executed
	s.rw.Lock()
	defer s.rw.Unlock()

	delete(s.Sessions, sessionID)
	_, err := db.Exec("DELETE FROM `sessions` WHERE `id` = ?", sessionID)
	return err
}

// DeleteAllUserSessions deletes all sessions for a user in internal session storage and database
func (s *structUserSession) DeleteAllUserSessions(userID uint64) error {
	// We need to wait for mysql to not allow session creation while mysql query is being executed
	s.rw.Lock()
	defer s.rw.Unlock()

	_, err := db.Exec("DELETE FROM `sessions` WHERE `userID` = ?", userID)

	for cookie, details := range s.Sessions {
		if details.UserID == userID {
			delete(s.Sessions, cookie)
		}
	}

	return err
}

// checkRefresh checks if the session needs a metadata refresh
func (s *structUserSession) checkRefresh(refreshTime time.Time) bool {
	if refreshTime.Add(time.Hour).Before(time.Now()) {
		return true
	}
	return false
}

// GetUserIDFromDBSession returns the userID from the session
func (s *structUserSession) GetUserIDFromDBSession(sessionID string) uint64 {
	var userID uint64
	err := db.QueryRow("SELECT IFNULL(`userID`, 0) FROM `sessions` WHERE `id` = ? LIMIT 1", sessionID).Scan(&userID)
	if err != nil {
		if config.Debug {
			log.Printf("Error getting userID from session %s: %s", sessionID, err.Error())
		}
		return userID
	}
	return userID
}

// GetUserUPNFromID returns the UPN from the user
func (s *structUserSession) GetUserUPNFromID(userID uint64) string {
	var userName string
	err := db.QueryRow("SELECT IFNULL(`userPrincipalName`, \"\") FROM `users` WHERE `id` = ? LIMIT 1", userID).Scan(&userName)
	if err != nil {
		log.Println("MySQL :: Error selecting userName from session", err.Error())
		return userName
	}
	return userName
}

// GetUserDNFromID returns the DN from the user
func (s *structUserSession) GetUserDNFromID(userID uint64) string {
	var userDN string
	err := db.QueryRow("SELECT IFNULL(`distinguishedName`, \"\") FROM `users` WHERE `id` = ? LIMIT 1", userID).Scan(&userDN)
	if err != nil {
		log.Println("MySQL :: Error selecting distinguishedName from session", err.Error())
		return userDN
	}
	return userDN
}

// Garbage collection for sessions
// delete old sessions from local storage and database if they are not used anymore
// refresh metadata of sessions if outdated (e.g. LDAP lookup if enabled)
func (s *structUserSession) GC() {
	s.rw.Lock()
	defer s.rw.Unlock()

	// define timeframe at which we remove old sessions
	expireHours := time.Duration(24 * 14) // 14 days default expiration
	if config.Session.ExpireHours > 0 {
		expireHours = time.Duration(config.Session.ExpireHours)
	}

	// loop through all sessions and check if they are outdated
	for sessionID, data := range s.Sessions {
		if data.RefreshTime.Add(time.Hour * expireHours).Before(time.Now()) {
			// delete entry from local map
			delete(s.Sessions, sessionID)
			// delete entry from database
			_, err := db.Exec("DELETE FROM `sessions` WHERE `id` = ?", sessionID)
			if err != nil {
				log.Println("MySQL :: Error deleting session", err.Error())
			}

			if config.Debug {
				log.Printf("Session %s expired", sessionID)
			}
		}
	}
}

// DeleteAllSessionsByUserID deletes all sessions for a specific user
func (s *structUserSession) DeleteAllSessionsByUserID(userID uint64) error {
	s.rw.Lock()
	defer s.rw.Unlock()

	// delete all entries from local map
	for sessionID, data := range s.Sessions {
		if data.UserID == userID {
			delete(s.Sessions, sessionID)
		}
	}

	// delete all entries from database
	_, err := db.Exec("DELETE FROM `sessions` WHERE `userID` = ?", userID)
	return err
}

// generate random bytes for session cookie
func (s *structUserSession) generateRandomBytes(length int) ([]byte, error) {
	bytes := make([]byte, length)
	_, err := rand.Read(bytes)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

// generate random string for session cookie
// OWASP recommends 64 bit of entropy for a session
// entropy is calculated as log2(possible values^length)
// possible values are 86 characters as of const letters
// 64 bit of entropy can be archived as follows:
//
//	length of 10 results in: log2(86^10) = 64.262647547 - bare minimum
//	length of 16 results in: log2(86^16) = 102.82023608 - good length
//	length of 20 results in: log2(86^20) = 128.52529509 - double of recommended length
//	length of 32 results in: log2(86^32) = 205.64047215 - more then triple of recommended length
//
// as of the above calculations, it's not recommended to use a length of 9 or less
// also a too long length is not recommended as it will result in a too long cookie and string comparison will be slower
// https://www.owasp.org/index.php/Session_Management_Cheat_Sheet
func (s *structUserSession) generateRandomString(length int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-_.,':!?@#$%^&()[]{}<>|"
	bytes, err := s.generateRandomBytes(length)
	if err != nil {
		return "", err
	}
	for i, bytePosition := range bytes {
		bytes[i] = letters[bytePosition%byte(len(letters))]
	}
	return string(bytes), nil
}

// GetLastUsed returns the last used time of a session
func (s *structSessionLastUsed) GetLastUsed(sessionID string) time.Time {
	s.rw.RLock()
	defer s.rw.RUnlock()
	return s.Sessions[sessionID]
}

// SetLastUsed sets the last used time of a session
func (s *structSessionLastUsed) SetLastUsed(sessionID string, lastUsed time.Time) {
	s.rw.Lock()
	defer s.rw.Unlock()

	// create sessions map if it doesn't exist
	if s.Sessions == nil {
		s.Sessions = make(map[string]time.Time)
	}

	s.Sessions[sessionID] = lastUsed
}

// DeleteLastUsed deletes the last used time of a session
func (s *structSessionLastUsed) DeleteLastUsed(sessionID string) {
	s.rw.Lock()
	defer s.rw.Unlock()
	delete(s.Sessions, sessionID)
}

// GC writes the last used time of all sessions to the database and deletes the old records
func (s *structSessionLastUsed) GC() {
	s.rw.Lock()
	defer s.rw.Unlock()

	// prepare MySQL statement
	stmt, err := db.Prepare("UPDATE `sessions` SET `last_used` = ? WHERE `id` = ?")
	if err != nil {
		log.Println("MySQL :: Error preparing statement for updating last_used", err.Error())
		return
	}

	// loop through all sessions and update last_used in database
	for sessionID, lastUsed := range s.Sessions {
		// insert last used time into database
		// if the session doesn't exist in the database, it will be deleted from the map
		golangDateTime := lastUsed.Format("2006-01-02 15:04:05")
		_, err := stmt.Exec(golangDateTime, sessionID)
		if err != nil {
			log.Println("MySQL :: Error updating last_used", err.Error())

			// delete session locally if we can't update the last_used time in the database
			if err := session.DeleteSession(sessionID); err != nil {
				log.Println("MySQL :: Error deleting session", err.Error())
			}
		}

		// delete entry from local map
		delete(s.Sessions, sessionID)

		if config.Debug {
			log.Printf("SessionLastUsed :: Updated last_used for session %s to %s", sessionID, golangDateTime)
		}
	}

	// close MySQL statement
	err = stmt.Close()
	if err != nil {
		log.Println("MySQL :: Error closing statement for updating last_used", err.Error())
	}
}
