package main

import (
	"html/template"
	"log"
	"net/http"
	"strings"
)

type errorPageInformation struct {
	Title   string
	Message string
}

//nolint:lll
const (
	definedErrorTitleDomainUnknown   = "Domain unknown"
	definedErrorMessageDomainUnknown = "The domain you requested isn't known and can't be served. Please check if the domain you entered is correct."
	definedErrorTitleProxyError      = "Proxy error"
	definedErrorMessageProxyError    = "The server wasn't able to reach the destination server. If the problem persists, please contact the administrator and report the following error message:\n\n%s"
	definedErrorTitleInternalError   = "Internal error"
	definedErrorMessageInternalError = "Something went wrong processing your request which caused an uncorrectable error. Please conect the administrator and provide the following error message:\n\n%s"
	definedErrorTitleUnauthorized    = "Unauthorized"
	definedErrorMessageUnauthorized  = "You do not have permission to view this page."
)

func httpWriteErrorPage(w http.ResponseWriter, info errorPageInformation) {
	t, ok := templates["error.html"]
	if !ok {
		log.Println("ERROR :: Can't find error.html error page from embed")
		return
	}

	// set noindex header
	w.Header().Set("X-Robots-Tag", "noindex")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")

	// Message may have newlines in it to indicate a line break
	// relace line breaks with <br> if no HTML is present
	if !strings.Contains(info.Message, "<") {
		info.Message = strings.ReplaceAll(info.Message, "\n", "<br>")
	}

	// create map for template
	data := make(map[string]interface{})
	data["Title"] = info.Title
	data["Message"] = template.HTML(info.Message) //nolint:gosec
	data["BaseDomain"] = config.BaseDomain
	data["Footer"] = template.HTML(config.AuthServer.Footer) // nolint: gosec

	if err := t.Execute(w, data); err != nil {
		log.Printf("ERROR :: Error executing template %s %s", "error.html", err.Error())

		// Fall back to plain text
		w.Write([]byte(
			"<!DOCTYPE html><html><head><title>" + info.Title + "</title></head><body><h1>" + info.Title + "</h1><p>" + info.Message + "</p></body></html>",
		))
	}
}
