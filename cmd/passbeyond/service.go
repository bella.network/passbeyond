package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
)

// listen for SIGUSR1 and reload the configuration
func listenForReload() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGUSR1)
	for range c {
		log.Println("Received SIGUSR1, reloading configuration")
		reloadConfiguration()
	}
}

// reloadConfiguration reloads the configuration from file
func reloadConfiguration() {
	log.Println("Reload appliation cache (domain mappings and settings)")
	cacheApplications.RenewCache()
}
