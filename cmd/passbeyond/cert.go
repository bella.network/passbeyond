package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"os"
	"time"
)

// generate certificate to be used for SSO
func generatePublicAndPrivateKey(baseDomain string) (*tls.Certificate, error) {
	// generate self-signed certificate for SSO
	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"PassBeyond"},
			CommonName:   "PassBeyond",
		},
		NotBefore: time.Now().Add(time.Hour * 24 * -1),                  // valid from yesterday
		NotAfter:  time.Now().Add(time.Hour*24*365*5 + time.Hour*24*14), // valid for 5 years and 2 weeks
		KeyUsage:  x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageServerAuth,
		},
		DNSNames: []string{
			baseDomain,
		},
		IsCA: false,
	}
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}
	pub := &priv.PublicKey
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pub, priv)
	if err != nil {
		return nil, err
	}

	// write x509 certificate into tls.certificate
	publicKey := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	privateKey := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})

	cert, err := tls.X509KeyPair(publicKey, privateKey)
	if err != nil {
		return nil, err
	}

	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	return &cert, err
}

// samlLoadKeyPair loads the public and private key for SAML authentication
func samlLoadKeyPair(settings samlKeySettings) (*tls.Certificate, error) {
	// check if file is configured to be used
	if settings.PublicKeyPath != "" && settings.PrivateKeyPath != "" {
		cert, err := tls.LoadX509KeyPair(settings.PublicKeyPath, settings.PrivateKeyPath)
		if err != nil {
			return nil, err
		}
		cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
		if err != nil {
			return nil, err
		}
		return &cert, nil
	}

	// no certificate given
	// as we require a certificate, check if a certificate exists which was previously generated
	// in this case, if the file does not exist generate a new self-signed certificate
	if _, err := os.Stat(settings.FallbackPublicKeyPath); err != nil {
		newCert, err := generatePublicAndPrivateKey(settings.FallbackDomain)
		if err != nil {
			return nil, err
		}

		// write private key to file
		privateKey := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(newCert.PrivateKey.(*rsa.PrivateKey))})
		err = os.WriteFile(settings.FallbackPrivateKeyPath, privateKey, 0600)
		if err != nil {
			return nil, err
		}

		// write public key to file
		publicKey := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: newCert.Certificate[0]})
		err = os.WriteFile(settings.FallbackPublicKeyPath, publicKey, 0600)
		if err != nil {
			return nil, err
		}

		return newCert, nil
	}

	// local fallback file is available, read it and return it as current certificate
	keyPairFromFS, err := tls.LoadX509KeyPair(settings.FallbackPublicKeyPath, settings.FallbackPrivateKeyPath)
	if err != nil {
		return nil, err
	}
	keyPairFromFS.Leaf, err = x509.ParseCertificate(keyPairFromFS.Certificate[0])
	if err != nil {
		return nil, err
	}

	return &keyPairFromFS, nil
}

type samlKeySettings struct {
	PublicKeyPath          string
	PrivateKeyPath         string
	FallbackPublicKeyPath  string
	FallbackPrivateKeyPath string
	FallbackDomain         string
}

// certCheckIfExpired checks if the certificate is expiring soon
func certCheckIfExpired(cert *tls.Certificate) bool {
	// Check if certificate was provided
	if cert == nil {
		return true
	}

	// Check if leaf is available
	if cert.Leaf == nil {
		// if no certificate is given we can't check if it is expired
		if cert.Certificate == nil {
			return true
		}

		// parse certificate into leaf
		var err error
		cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
		if err != nil {
			return true
		}
	}

	return cert.Leaf.NotAfter.Before(time.Now().Add(time.Hour * 24 * 14))
}
