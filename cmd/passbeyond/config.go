package main

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

// base settings for the application
type programSettings struct {
	BaseDomain string             `yaml:"basedomain"`
	Debug      bool               `yaml:"debug,omitempty"`
	SAML       settingsSAML       `yaml:"saml"`
	AuthServer settingsAuthServer `yaml:"authServer"`
	Proxy      settingsProxy      `yaml:"proxy"`
	Session    settingsSession    `yaml:"session"`
	Database   settingsDatabase   `yaml:"database"`
	LDAP       settingsLDAP       `yaml:"ldap"`
}

// settingsSAML contains the settings for SAML authentication
type settingsSAML struct {
	IdpMetadataURL string `yaml:"idpMetadataURL"` // URL to the SAML IdP metadata
	PublicKey      string `yaml:"publickey"`      // Public key for SAML authentication
	PrivateKey     string `yaml:"privatekey"`     // Private key for SAML authentication
	UserCreation   bool   `yaml:"usercreation"`   // Allows creation of users using SAML
	CacheMetadata  bool   `yaml:"cacheMetadata"`  // Cache the metadata from the IdP
}

// settingsLDAP contains the settings for LDAP authentication and user verification
type settingsLDAP struct {
	Server   string `yaml:"server"`           // LDAP server address or domain, e.g. ldap.example.com
	Port     int    `yaml:"port"`             // LDAP server port, when using 636 set LDAPS to true
	LDAPS    bool   `yaml:"ldaps"`            // Use LDAPS for TLS encrypted connections
	BindDN   string `yaml:"bindDN"`           // LDAP bind DN, e.g. CN=binduser,OU=Users,DC=example,DC=com
	Password string `yaml:"password"`         // LDAP password
	BaseDN   string `yaml:"baseDN"`           // LDAP base DN, e.g. OU=Company,DC=example,DC=com
	ADSite   string `yaml:"adSite,omitempty"` // Active Directory site name, e.g. Default-First-Site-Name
}

type settingsDatabase struct {
	Server   string `yaml:"server"` // MySQL server address with port, e.g. localhost:3306
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

type settingsProxy struct {
	StripResponseHeaders     bool `yaml:"stripResponseHeaders"`     // Strip headers like X-Powered-By, Server, ...
	SendPerformanceData      bool `yaml:"sendPerformanceData"`      // Send performance data like response time of backend server, MySQL lookup time, ...
	AlwaysAllowACMEChallenge bool `yaml:"alwaysAllowACMEChallenge"` // Passtrough ACME challenges to the backend server without authentication
}

type settingsSession struct {
	ExpireHours int `yaml:"expireHours"` // Session expiration time in hours
}

type settingsAuthServer struct {
	APIEndpoint              bool     `yaml:"apiEndpoint"`                 // Enable API endpoint to query user data
	APIAllowedOrigins        []string `yaml:"allowedOrigins"`              // Allowed origins for API endpoint
	StartPageRedirect        string   `yaml:"startPageRedirect,omitempty"` // Redirect to this URL instead of showing the start page
	StartPageApplicationList bool     `yaml:"startPageApplicationList"`    // Not implemented yet
	InternalIPs              []string `yaml:"internalIPs"`                 // Internals IPs which are allowed to skip authentication
	Footer                   string   `yaml:"footer,omitempty"`            // Custom footer text on error pages
}

var config programSettings

// Read the configuration file and return the settings as global variable
func readConfig() {
	// read from globally defined config file
	content, err := os.ReadFile("/etc/passbeyond/config.yaml")
	if err != nil {
		// if not available, try to read from current directory
		content, err = os.ReadFile("config.yaml")
		if err != nil {
			log.Println("Can't read settings from config file. Assuming defaults")
			return
		}
	}

	// unmarshal the yaml file into the settings struct
	if err = yaml.Unmarshal(content, &config); err != nil {
		log.Fatalf("Can't read settings from config file: %v", err)
	}

	// check if required settings are set
	if config.LDAP.BaseDN == "" {
		log.Fatalf("LDAP base DN not set in config file: config:LDAP:baseDN")
	}
	if config.LDAP.BindDN == "" {
		log.Fatalf("LDAP bind DN not set in config file: config:LDAP:bindDN")
	}
	if config.LDAP.Password == "" {
		log.Fatalf("LDAP password not set in config file: config:LDAP:password")
	}
	if config.LDAP.Server == "" {
		log.Fatalf("LDAP server not set in config file: config:LDAP:server")
	}

	if config.SAML.IdpMetadataURL == "" {
		log.Fatalf("SAML IdP metadata URL not set in config file: config:SAML:idpMetadataURL")
	}

	if config.BaseDomain == "" {
		log.Fatalf("Base domain not set in config file: config:baseDomain")
	}

	if config.Database.Database == "" {
		log.Fatalf("Database name not set in config file: config:database:database")
	}
	if config.Database.Password == "" {
		log.Fatalf("Database password not set in config file: config:database:password")
	}
	if config.Database.Server == "" {
		log.Fatalf("Database server not set in config file: config:database:server")
	}
	if config.Database.Username == "" {
		log.Fatalf("Database username not set in config file: config:database:username")
	}
}
