package main

import (
	"log"
	"sync"
	"time"
)

// cacheApplications is a cache for all applications
var cacheApplications typeApplicationsCache

// typeApplicationsCache is a cache for all applications
type typeApplicationsCache struct {
	LastRenew time.Time
	Domains   map[string]applicationDomain
	rw        sync.RWMutex
}

// applicaitonDomain is a single domain configuration fetched from database
type applicationDomain struct {
	ID                       int64
	Name                     string
	Domain                   string
	Destination              string
	TrustedNetworkPasstrough bool
	SkipTLSVerification      bool
	BasicAuthorization       bool
}

// RenewCache will renew the cache of available applications
func (t *typeApplicationsCache) RenewCache() {
	// select data from database containing all domains and destinations with additional information
	rows, err := db.Query("SELECT `id`, `name`, `domain`, `destination`, `trustedNetworkPasstrough`, `skipTLSVerification`, `BasicAuthorization` " +
		"FROM `applications` ORDER BY `domain`")
	if err != nil {
		log.Println("MySQL :: Error seleting data from database", err.Error())
		return
	}
	defer rows.Close()

	if rows.Err() != nil {
		log.Println("MySQL :: Error reading data from database", rows.Err().Error())
		return
	}

	// create list of old domains
	var oldDomains []string
	t.rw.RLock()
	for domain := range t.Domains {
		oldDomains = append(oldDomains, domain)
	}
	t.rw.RUnlock()

	// Get data of each row and build a new table containing all domains and destinations
	newDomains := make(map[string]applicationDomain, 0)
	for rows.Next() {
		var singleApplication applicationDomain

		if err := rows.Scan(&singleApplication.ID,
			&singleApplication.Name,
			&singleApplication.Domain,
			&singleApplication.Destination,
			&singleApplication.TrustedNetworkPasstrough,
			&singleApplication.SkipTLSVerification,
			&singleApplication.BasicAuthorization,
		); err != nil {
			log.Println("MySQL :: Error scanning data", err)
			break
		}

		newDomains[singleApplication.Domain] = singleApplication
	}

	// determine domains to be removed
	var domainsToRemove []string
	var domainsToAdd []string
	for _, oldDomain := range oldDomains {
		// generate list of domains to be removed
		var found bool
		for _, newDomain := range newDomains {
			if oldDomain == newDomain.Domain {
				found = true
				break
			}
		}

		if !found {
			domainsToRemove = append(domainsToRemove, oldDomain)
		}
	}

	// determine domains to be added
	for _, newDomain := range newDomains {
		// generate list of domains to be added
		var found bool
		for _, oldDomain := range oldDomains {
			if newDomain.Domain == oldDomain {
				found = true
				break
			}
		}

		if !found {
			domainsToAdd = append(domainsToAdd, newDomain.Domain)
		}
	}

	// build new list of domains
	var domainsList map[string]applicationDomain
	t.rw.RLock()
	domainsList = t.Domains
	t.rw.RUnlock()

	// create new list of domains if not already done
	if domainsList == nil {
		domainsList = make(map[string]applicationDomain, 0)
	}

	// remove domains
	for _, domain := range domainsToRemove {
		delete(domainsList, domain)
		go func(domain string) {
			delete(hostProxy, domain)
		}(domain)
	}

	// add domains
	for _, domain := range domainsToAdd {
		domainsList[domain] = newDomains[domain]
	}

	// write new data to cache
	t.rw.Lock()
	t.Domains = domainsList
	t.LastRenew = time.Now()
	t.rw.Unlock()
}

// GetDestinationForDomain will return the destination for a given domain
func (t *typeApplicationsCache) GetDestinationForDomain(domain string) string {
	t.rw.RLock()
	defer t.rw.RUnlock()

	if dataset, ok := t.Domains[domain]; ok {
		return dataset.Destination
	}

	return ""
}

// GetDomain will return the applicationDomain struct of a given domain
func (t *typeApplicationsCache) GetDomain(domain string) applicationDomain {
	t.rw.RLock()
	defer t.rw.RUnlock()

	if dataset, ok := t.Domains[domain]; ok {
		return dataset
	}

	return applicationDomain{}
}
