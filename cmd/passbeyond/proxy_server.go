package main

import (
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var allowedCIDR []net.IPNet

// Proxy mappings to use existing tunnel and not to recreate new ones
var hostProxy map[string]*httputil.ReverseProxy

func parseInternalIPList() {
	// Read internal CIDR ranges from config
	for _, cidr := range config.AuthServer.InternalIPs {
		// parse CIDR and add to allowedCIDR list
		_, ipnet, err := net.ParseCIDR(cidr)
		if err != nil {
			log.Fatalf("Can't parse internal IP %s: %v", cidr, err)
		}
		allowedCIDR = append(allowedCIDR, *ipnet)
	}

	// init proxy mapping
	hostProxy = make(map[string]*httputil.ReverseProxy)
}

// passbeyondHandler handles session request and resumption
// this isusually on the /__passbeyond-session path on the proxied service
func passbeyondHandler(w http.ResponseWriter, r *http.Request) {
	// Check if source GET parameter is set and included domain is allowed
	// if the domain in source does not match the currently requested domain
	// the source is overridden with the current domain and path set to /
	redirectionSource := ""
	unescapedSource := ""
	keys, ok := r.URL.Query()["source"]
	if ok && len(keys) == 1 {
		unescapedURL, err := url.QueryUnescape(keys[0])
		if err == nil {
			parsedURL, err := url.Parse(unescapedURL)
			if err == nil && parsedURL.Host == r.Host {
				// check if __passbeyond-session is is in path to prevent loops
				if !strings.Contains(parsedURL.Path, "__passbeyond-session") {
					redirectionSource = url.QueryEscape(parsedURL.String())
					unescapedSource = parsedURL.String()
				}
			}
		}
	}

	// No redirection given, use base domain
	if redirectionSource == "" {
		redirectionSource = url.QueryEscape("https://" + r.Host + "/")
	}

	// Cookie GET parameter is set, this usually indicates that we were redirected back from the authentication server
	// if the cookie is set, we check if the cookie is valid and if so, we set the session cookie and redirect to the source
	keys, ok = r.URL.Query()["cookie"]
	if ok && len(keys) == 1 {
		if uid, err := session.CheckValidSession(keys[0]); uid > 0 && err == nil {
			// Extend lifespan of cookie
			http.SetCookie(w, &http.Cookie{
				Name:     sessionCookieName,
				Value:    keys[0],
				Expires:  time.Now().Add(time.Hour * 24 * 7 * 8),
				Secure:   true,
				HttpOnly: true,
				Path:     "/",
				SameSite: http.SameSiteNoneMode,
			})

			// check if source is set and redirect to it if valid
			if unescapedSource != "" {
				http.Redirect(w, r, unescapedSource, http.StatusTemporaryRedirect)
			} else {
				http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			}
			return
		}
	}

	// We got no cookie, we have to request it from the base authetication service
	// determine source domain for redirection
	http.Redirect(w, r, "https://"+config.BaseDomain+"/pass-session?source="+redirectionSource, http.StatusTemporaryRedirect)
}

func globalHandler(w http.ResponseWriter, r *http.Request) {
	requestStart := time.Now()

	// check if domain is permitted to be forwarded
	destination := cacheApplications.GetDomain(r.Host)
	if destination.ID == 0 {
		w.WriteHeader(http.StatusNotFound)
		if config.Proxy.SendPerformanceData {
			w.Header().Set("Server-Timing", "passbeyond-appcheck;dur="+strconv.FormatInt(time.Since(requestStart).Milliseconds(), 10))
		}
		httpWriteErrorPage(w, errorPageInformation{
			Title:   definedErrorTitleDomainUnknown,
			Message: definedErrorMessageDomainUnknown,
		})
		return
	}

	// get client IP address
	clientIP := r.Header.Get("X-Forwarded-For")
	if clientIP == "" {
		clientIP = r.RemoteAddr
	}

	// clientIP may contain multiple IPs, if it does, take the first one
	if strings.Contains(clientIP, ",") {
		clientIP = strings.Split(clientIP, ",")[0]
		clientIP = strings.TrimSpace(clientIP)
	}

	// set client IP address into X-REAL-IP header
	r.Header.Set("X-REAL-IP", clientIP)

	// set protocol to determine if using http or https
	r.Header.Set("X-Forwarded-Proto", "https")

	// if passtrough is enabled, do not check for authentication
	var passtrough bool

	// check if request comes from a trusted network
	if destination.TrustedNetworkPasstrough {
		clientIPParsed := net.ParseIP(clientIP)
		for _, singleNet := range allowedCIDR {
			if singleNet.Contains(clientIPParsed) {
				passtrough = true
				break
			}
		}
	}

	// if AlwaysAllowACMEChallenge is set, pass through all requests for ACME challenge
	if config.Proxy.AlwaysAllowACMEChallenge && strings.HasPrefix(r.URL.Path, "/.well-known/acme-challenge/") {
		passtrough = true
	}

	// save the users ID if it can be determined
	var currentSession uint64

	beforeSessionCheck := time.Now()

	// if request shoulnd't be passed through, check if user is authenticated
	// if not, redirect to authentication service
	if !passtrough {
		var err error
		currentSession, err = session.CheckValidHTTPSession(r)
		if err != nil || currentSession == 0 {
			// Redirect to get session cookie. Abort further processing to not allow hidden attacks to target
			if config.Proxy.SendPerformanceData {
				w.Header().Set("Server-Timing", fmt.Sprintf("passbeyond-appcheck;dur=%d, passbeyond-session;dur=%d",
					beforeSessionCheck.Sub(requestStart).Milliseconds(),
					time.Since(requestStart).Milliseconds(),
				))
			}

			// Redirect without source
			http.Redirect(w, r, "/__passbeyond-session?source="+url.QueryEscape("https://"+r.Host+r.URL.RequestURI()), http.StatusTemporaryRedirect)

			log.Printf("UNAUTH-REDIRECT %s %s", clientIP, destination.Domain)
			return
		}
	}

	beforeProxyDetermination := time.Now()

	// Handle requests with existing proxy chain
	if fn, ok := hostProxy[destination.Domain]; ok {
		r.Header.Del(sessionCookieName)
		r.Header.Del("X-REMOTE-USER")

		// delete single cookie from headers by creating a new cookie header
		cookieHeader := r.Header.Get("Cookie")
		sessionCookieContent, err := r.Cookie(sessionCookieName)
		if err == nil {
			cookieHeader = strings.ReplaceAll(cookieHeader, sessionCookieName+"="+sessionCookieContent.Value, "")
			cookieHeader = strings.ReplaceAll(cookieHeader, sessionCookieName+"=\""+sessionCookieContent.Value, "\"") // iOS Safari adds quotes to cookie value
			cookieHeader = strings.TrimPrefix(cookieHeader, ";")
			cookieHeader = strings.TrimSuffix(cookieHeader, ";")
			cookieHeader = strings.Trim(cookieHeader, " ")

			if cookieHeader == "" || cookieHeader == "\"\"" {
				r.Header.Del("Cookie")
			} else {
				r.Header.Set("Cookie", cookieHeader)
			}
		}

		// Add userid to identify the user, -1 is in passtrough mode
		r.Header.Del("X-PASSBEYOND-UID")
		if !passtrough {
			// FIXME: Add data here which should be passed to the proxied application
			r.Header.Set("X-PASSBEYOND-UID", strconv.FormatUint(currentSession, 10))
			r.Header.Set("X-PASSBEYOND-AUTH", "user")
			r.Header.Set("X-REMOTE-USER", users.GetUPN(currentSession))
			r.Header.Set("X-SAMACCOUNTNAME", users.GetSAMAccountName(currentSession))

			// send authoization header if configured
			if destination.BasicAuthorization {
				r.Header.Set("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(users.GetUPN(currentSession)+":")))
			}

			// set time when session was last used the last time
			go sessionLastUsed.SetLastUsed(sessionCookieContent.Value, time.Now())
		} else {
			r.Header.Set("X-PASSBEYOND-UID", "-1")
			r.Header.Set("X-PASSBEYOND-AUTH", "passtrough")
		}

		// Update current session LastUse timestamp
		if passtrough {
			log.Printf("PASSTROUGH %s %s", clientIP, destination.Domain)
		} else {
			log.Printf("AUTH %s %s", clientIP, destination.Domain)
		}

		if config.Proxy.SendPerformanceData {
			w.Header().Set("Server-Timing", fmt.Sprintf("passbeyond-appcheck;dur=%d, passbeyond-session;dur=%d, passbeyond-proxy;dur=%d",
				beforeSessionCheck.Sub(requestStart).Milliseconds(),
				beforeProxyDetermination.Sub(beforeSessionCheck).Milliseconds(),
				time.Since(requestStart).Milliseconds(),
			))
		}

		fn.ServeHTTP(w, r)
		return
	}

	// Proxy chain does not exist, create a new one
	remoteURL, err := url.Parse(destination.Destination)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		httpWriteErrorPage(w, errorPageInformation{
			Title:   definedErrorTitleInternalError,
			Message: fmt.Sprintf(definedErrorMessageInternalError, err.Error()),
		})
		log.Println("target parse fail:", err)
		return
	}

	// Build reverse proxy for this connection
	proxy := httputil.NewSingleHostReverseProxy(remoteURL)

	// Modify request data
	proxy.Transport = &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 2 * time.Second,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: destination.SkipTLSVerification, //nolint:gosec
		},
	}

	// strip response headers from remote to reduce information leakage
	if config.Proxy.StripResponseHeaders {
		proxy.ModifyResponse = func(resp *http.Response) error {
			// Remove headers that might leak information
			resp.Header.Del("X-Powered-By")        // PHP, ASP.NET, etc.
			resp.Header.Del("Server")              // Apache, nginx, etc.
			resp.Header.Del("X-AspNet-Version")    // ASP.NET
			resp.Header.Del("X-AspNetMvc-Version") // ASP.NET MVC
			resp.Header.Del("X-SourceFiles")       // ASP.NET

			return nil
		}
	}

	// Add error handler to display error page on proxy errors
	proxy.ErrorHandler = proxyError

	log.Printf("CREATE %s", destination.Domain)
	hostProxy[destination.Domain] = proxy

	if config.Proxy.SendPerformanceData {
		w.Header().Set("Server-Timing", fmt.Sprintf("passbeyond-appcheck;dur=%d, passbeyond-session;dur=%d, passbeyond-proxy;dur=%d",
			beforeSessionCheck.Sub(requestStart).Milliseconds(),
			beforeProxyDetermination.Sub(beforeSessionCheck).Milliseconds(),
			time.Since(requestStart).Milliseconds(),
		))
	}

	// call itself as proxy is now build and can be used
	globalHandler(w, r)
}

func httpProxyServer() {
	proxyServer := http.NewServeMux()

	// Global override as handler for sessions
	// This is used to set the session cookie
	// Address contains the service name to be unique as possible to not collide with other services
	proxyServer.HandleFunc("/__passbeyond-session", passbeyondHandler)

	// Default path which matches everything
	proxyServer.HandleFunc("/", globalHandler)

	server := &http.Server{
		ReadTimeout:       10 * time.Second,
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      10 * time.Second,
		MaxHeaderBytes:    1 << 20,
		Addr:              ":5443",
		Handler:           proxyServer,
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func proxyError(w http.ResponseWriter, r *http.Request, err error) {
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Referrer-Policy", "same-origin")

	// set headers that this page is not cacheable
	w.Header().Set("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")

	w.WriteHeader(http.StatusGatewayTimeout)
	httpWriteErrorPage(w, errorPageInformation{
		Title:   definedErrorTitleProxyError,
		Message: fmt.Sprintf(definedErrorMessageProxyError, err.Error()),
	})

	log.Printf("PROXY-ERROR: %s %s - ERROR: %s", r.RemoteAddr, r.Host, err)
}
