package main

import (
	"errors"
	"log"
	"sync"
	"time"
)

var users Users

type Users struct {
	Users map[uint64]structUser `json:"users"`
	rw    sync.RWMutex
}

type structUser struct {
	UserID         uint64    `json:"userID"`
	Name           string    `json:"name"`
	UPN            string    `json:"UPN"`
	DN             string    `json:"DN"`
	SAMAccountName string    `json:"sAMAccountName"`
	LastRefresh    time.Time `json:"lastRefresh"`
	Groups         []uint64  `json:"groups"`
}

// Add adds user to internal list
func (u *Users) Add(uid uint64, settings structUser) {
	u.rw.Lock()
	defer u.rw.Unlock()

	if u.Users == nil {
		u.Users = make(map[uint64]structUser)
	}

	u.Users[uid] = structUser{
		UserID:         uid,
		Name:           settings.Name,
		UPN:            settings.UPN,
		DN:             settings.DN,
		SAMAccountName: settings.SAMAccountName,
		LastRefresh:    time.Now(),
		Groups:         settings.Groups,
	}
}

// AddFromExternalSource adds user to internal list from MySQL database
// by importing all information like UPN, DN, groups, etc.
func (u *Users) AddFromExternalSource(uid uint64) error {
	// Fetch user information from MySQL
	var name, upn, dn, san string
	err := db.QueryRow("SELECT `name`, `userPrincipalName`, `distinguishedName`, `sAMAccountName` FROM `users` WHERE `id` = ?", uid).Scan(&name, &upn, &dn, &san)
	if err != nil {
		return err
	}

	// Fetch user groups from MySQL
	var groups []uint64
	rows, err := db.Query("SELECT `groupID` FROM `user_groups` WHERE `userID` = ?", uid)
	if err != nil {
		return err
	}

	for rows.Next() {
		var groupID uint64
		err = rows.Scan(&groupID)
		if err != nil {
			return err
		}

		groups = append(groups, groupID)
	}

	// Check if user is not disabled in LDAP
	enabled, err := ldapCheckUserEnabled(upn)
	if err != nil {
		return err
	}

	if !enabled {
		return errors.New("user is disabled")
	}

	// Add user to internal list
	u.Add(uid, structUser{
		UserID:         uid,
		Name:           name,
		UPN:            upn,
		DN:             dn,
		SAMAccountName: san,
		LastRefresh:    time.Now(),
		Groups:         groups,
	})

	return nil
}

// ExistsInDatabase checks if user exists in database by UPN
func (u *Users) ExistsInDatabase(userPrincipalName string) bool {
	var id uint64
	if err := db.QueryRow("SELECT `id` FROM `users` WHERE `userPrincipalName` = ?", userPrincipalName).Scan(&id); err != nil {
		return false
	}

	return id != 0
}

// ExistsInDatabaseBySAMAccountName checks if user exists in database by SAMAccountName
func (u *Users) ExistsInDatabaseBySAMAccountName(sAMAccountName string) bool {
	var id uint64
	if err := db.QueryRow("SELECT `id` FROM `users` WHERE `sAMAccountName` = ?", sAMAccountName).Scan(&id); err != nil {
		return false
	}

	return id != 0
}

func (u *Users) GetLastDatabaseRefresh(uid uint64) (time.Time, error) {
	var lastRefresh time.Time
	if err := db.QueryRow("SELECT `refresh` FROM `users` WHERE `id` = ?", uid).Scan(&lastRefresh); err != nil {
		return time.Time{}, err
	}

	return lastRefresh, nil
}

// AddGroup adds group id to user (only internal cache, not persisted to database)
func (u *Users) AddGroup(uid uint64, gid uint64) error {
	u.rw.Lock()
	defer u.rw.Unlock()

	if u.Users == nil {
		u.Users = make(map[uint64]structUser)
	}

	userObject := u.Users[uid]
	if userObject.UserID == 0 {
		return errors.New("user not found")
	}

	userObject.Groups = append(userObject.Groups, gid)
	userObject.LastRefresh = time.Now()
	u.Users[uid] = userObject

	return nil
}

// DeleteGroup deletes group id from user (only internal cache, not persisted to database)
func (u *Users) DeleteGroup(uid uint64, gid uint64) error {
	u.rw.Lock()
	defer u.rw.Unlock()

	if u.Users == nil {
		u.Users = make(map[uint64]structUser)
	}

	userObject := u.Users[uid]
	if userObject.UserID == 0 {
		return errors.New("user not found")
	}

	for i, groupID := range userObject.Groups {
		if groupID == gid {
			userObject.Groups = append(userObject.Groups[:i], userObject.Groups[i+1:]...)
			break
		}
	}

	userObject.LastRefresh = time.Now()
	u.Users[uid] = userObject

	return nil
}

// UserExists checks if user exists in internal list
func (u *Users) UserExists(uid uint64) bool {
	u.rw.RLock()
	defer u.rw.RUnlock()

	_, ok := u.Users[uid]
	return ok
}

// GetGroups returns all groups for user
func (u *Users) GetGroups(uid uint64) []uint64 {
	u.rw.RLock()
	defer u.rw.RUnlock()
	return u.Users[uid].Groups
}

// RefreshUserGroupsUsingLDAP refreshes user groups using LDAP
// connects to LDAP and adds known groups (nested search) to mapping table
func (u *Users) RefreshUserGroupsUsingLDAP(uid uint64) error {
	// get user details from LDAP
	if !u.UserExists(uid) {
		return errors.New("user not found")
	}

	// fetch all groups from LDAP
	groups, err := ldapFetchAllUsersGroupsByDN(u.GetDN(uid))
	if err != nil {
		return err
	}

	// check if group exists in database
	// and only add known ones to list
	var knownGroups []uint64
	for _, group := range groups {
		groupID, err := u.GetGroupID(group)
		if err == nil {
			knownGroups = append(knownGroups, groupID)
		}
	}

	// update user object
	u.rw.Lock()
	defer u.rw.Unlock()
	userObject := u.Users[uid]
	userObject.Groups = knownGroups
	userObject.LastRefresh = time.Now()
	u.Users[uid] = userObject

	return nil
}

func (u *Users) GetLastRefresh(uid uint64) time.Time {
	u.rw.RLock()
	defer u.rw.RUnlock()
	return u.Users[uid].LastRefresh
}

// GetGroupName returns group name by ID from the database
// function does not use any cache
func (u *Users) GetGroupName(gid uint64) (string, error) {
	// select group name from database
	var name string
	err := db.QueryRow("SELECT `name` FROM `groups` WHERE `id` = ?", gid).Scan(&name)
	if err != nil {
		return "", err
	}

	return name, nil
}

// GetGroupID returns group ID by name from the database
// function does not use any cache
func (u *Users) GetGroupID(name string) (uint64, error) {
	// select group ID from database
	var id uint64
	err := db.QueryRow("SELECT `id` FROM `groups` WHERE `name` = ?", name).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

// GetUPN returns user's UPN by ID
func (u *Users) GetUPN(uid uint64) string {
	u.rw.RLock()
	defer u.rw.RUnlock()
	return u.Users[uid].UPN
}

// GetDN returns user's DN by ID
func (u *Users) GetDN(uid uint64) string {
	u.rw.RLock()
	defer u.rw.RUnlock()
	return u.Users[uid].DN
}

// GetUserID returns user ID from internal list by given UPN
func (u *Users) GetUserID(upn string) uint64 {
	u.rw.RLock()
	defer u.rw.RUnlock()

	for _, user := range u.Users {
		if user.UPN == upn {
			return user.UserID
		}
	}

	return 0
}

// GetSAMAccountName returns user's SAMAccountName by ID
func (u *Users) GetSAMAccountName(uid uint64) string {
	u.rw.RLock()
	defer u.rw.RUnlock()
	return u.Users[uid].SAMAccountName
}

// Delete removes user from internal list
func (u *Users) Delete(uid uint64) {
	u.rw.Lock()
	defer u.rw.Unlock()
	delete(u.Users, uid)
}

// DeleteGroupFromDatabaseByUserID deletes group from database by user ID
func (u *Users) DeleteGroupFromDatabaseByUserID(uid uint64) {
	_, err := db.Exec("DELETE FROM `users_groups` WHERE `userID` = ?", uid)
	if err != nil {
		log.Printf("Failed to delete user's groups from database: %s", err.Error())
	}
}

// CleanupDataByUserID deletes user's data from database and internal list
func (u *Users) CleanupDataByUserID(uid uint64) {
	// delete all active sessions of user in database
	if err := session.DeleteAllSessionsByUserID(uid); err != nil {
		log.Printf("Failed to delete user sessions: %s", err.Error())
	}

	// delete all assigned groups of user in database
	u.DeleteGroupFromDatabaseByUserID(uid)

	// delete user from database
	if _, err := db.Exec("DELETE FROM `users` WHERE `id` = ?", uid); err != nil {
		log.Printf("Failed to delete user from database: %s", err.Error())
	}

	// delete all entries from internal lists
	u.Delete(uid)

	if config.Debug {
		log.Printf("Removed user %d from database", uid)
	}
}

// GC garbage collects users from internal list
func (u *Users) GC() {
	u.rw.Lock()
	defer u.rw.Unlock()

	for uid, user := range u.Users {
		if time.Since(user.LastRefresh) > time.Hour*24*7 {
			delete(u.Users, uid)

			if config.Debug {
				log.Printf("Removed user %s from internal users list", user.UPN)
			}
		}
	}
}

// GCDatabase garbage collects users from database
func (u *Users) GCDatabase() {
	// get all users from database
	rows, err := db.Query("SELECT `id`, `userPrincipalName`, `distinguishedName`, `sAMAccountName`, `name`, `refresh` FROM `users`")
	if err != nil {
		log.Printf("Failed to get users from database: %s", err.Error())
		return
	}
	defer rows.Close()

	if rows.Err() != nil {
		log.Printf("Failed to get users from database: %s", rows.Err().Error())
		return
	}

	// iterate over all users
	for rows.Next() {
		var (
			uid            uint64
			upn            string
			dn             string
			sAMAccountName string
			name           string
			refresh        time.Time
		)

		// scan user data
		if err := rows.Scan(&uid, &upn, &dn, &sAMAccountName, &name, &refresh); err != nil {
			log.Printf("Failed to scan user data: %s", err.Error())
			continue
		}

		// check if last refresh is older than 1 hour
		if time.Since(refresh) > time.Minute*60 {
			// fetch record from LDAP
			userinfo, err := ldapFetchUserByDN(dn)

			// account is disabled, delete all entries
			if err == errUserDisabled || !userinfo.Enabled {
				if config.Debug {
					log.Printf("User %s is disabled, deleting all entries", dn)
				}
				u.CleanupDataByUserID(uid)
			}

			if err != nil {
				log.Printf("Failed to fetch user %s from LDAP: %s", dn, err.Error())
				continue
			}

			// update user fields in database
			if _, err := db.Exec("UPDATE `users` SET `userPrincipalName` = ?, `distinguishedName` = ?, `sAMAccountName` = ?, `name` = ?, `refresh` = ? WHERE `id` = ?", userinfo.UserPrincipalName, userinfo.DN, userinfo.SAMAccountName, userinfo.CN, time.Now(), uid); err != nil {
				log.Printf("Failed to update user %s in database: %s", dn, err.Error())
				continue
			} else {
				if config.Debug {
					log.Printf("Updated user %s in database", dn)
				}
			}
		}
	}
}
