package main

import "testing"

func TestStringInSlice(t *testing.T) {
	slice := []string{"a", "b", "c"}
	if !stringInSlice("a", slice) {
		t.Error("Expected true, got false")
	}
	if stringInSlice("d", slice) {
		t.Error("Expected false, got true")
	}
}
