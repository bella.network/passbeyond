# PassBeyond

PassBeyond is a simple SAML SP nearly no own web interface. It is designed to be used with a reverse proxy like nginx or apache to provide a simple SAML login for your applications.
It is especially designed to be used as overlay for existing applications that do not support SAML authentication.

The main goal is to provide a authentication layer for applications, where the user has to authenticate only once and then can access all applications. These applications are only accessible after a successful authentication. The user is not able to access the applications directly.

## Features

- SAML 2.0 (Support for AD FS 3.0 and Azure AD)
  - Single Sign On
  - Single Logout
  - Attribute Mapping
  - Automatic Metadata Refresh
- Seamless Multidomain Support
  - Cross-Domain sessions
- LDAP (Active Directory)
  - Group Membership
  - User Lookup (check if user exists, is enabled, ...)
  - AD-Site Lookup Support
- Strong Session Management (200+ bit entropy)
  - Automatic Session Renewal
  - Session Timeout and Session Expiration
  - Session Cookie is HttpOnly and Secure
  - Session Usage Tracking
  - Cross-Domain Support
- Reverse Proxy Support
  - X-Forwarded-For#
  - X-Real-IP
  - X-Forwarded-Proto
  - X-Remote-User
  - X-Passbeyond-UID
  - Server Timing
  - Stripping of sensitive headers
    - X-Powered-By
    - Server
    - X-AspNet-Version
    - X-AspNetMvc-Version
    - X-SourceFiles
- API (REST, CORS, Get current User Information like Groups, Attributes, Picture, ...)

## Example applications that were successfully tested with PassBeyond

- Grafana
- Confluence (except Mobile App)
- Jira (except Mobile App)
- Syncthing
- Firefly III
- Proxmox
- AdGuard Home
- Guacamole
- Bitwarden/VaultWarden (except Mobile App)
- wger
- Graylog
- UniFi Controller
- Nextcloud (except Mobile App and Sync Client)
- SonarQube
- CheckMK
- Checkpoint Firewall
- Frigate

## TODO

- Logout
- Configure TLS certs and available internal URLs with config.json
- Make multi endpoint capable (redirect based on email domain to proper SAML endpoint)


```
c:[Type == "http://schemas.microsoft.com/ws/2008/06/idendity/claims/windowsaccountname", Issuer == "AD AUTHORITY"]
 => add(store = "Active Directory", types = ("http://schemas.xmlsoap.org/claims/Groups"), query = ";tokenGroups;{0}", param = c.Value);
```
