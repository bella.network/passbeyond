#!/usr/bin/env bash
# Create a .deb package for the current project
#
# This script is intended to be run from the project root directory.
# It will create a .deb package for the current project.

set -e

export BINARY_FILE=${1}
export BINARY_NAME=${2}
export BINARY_ARCH=${3}

# Determine new version and set it
export PKGNAME=$(grep "Package:" source/DEBIAN/control | cut -d " " -f2)
export MAINVERSION=$(grep "Version:" source/DEBIAN/control | cut -d " " -f2)
export DATE=$(date +%Y%m%d%H%M)
export SHORTVERS=$(echo $CI_COMMIT_SHA | cut -c 1-8)
export NEWVERSION="${MAINVERSION}-${DATE}+${SHORTVERS}"
sed -i -e "/^Version/c\Version\x3a ${NEWVERSION}" source/DEBIAN/control
sed -i -e "/^Installed-Size/c\Installed-Size\x3a $(du -s --exclude=DEBIAN source/ | cut -f 1)" source/DEBIAN/control

# Generate changelog based on last commits or tag
echo "${PKGNAME} (${NEWVERSION}) stable; urgency=low" > source/DEBIAN/changelog
echo "" >> source/DEBIAN/changelog
echo "  * Automatically generated with GitLab CI - ${LASTLOG}" >> source/DEBIAN/changelog
echo "" >> source/DEBIAN/changelog
echo " -- Thomas Bella <thomas+deb@bella.network>  $(date '+%a, %d %b %Y %H:%M:%S %z')" >> source/DEBIAN/changelog
echo "" >> source/DEBIAN/changelog
cat source/DEBIAN/changelog

# Move files
cp build/${BINARY_FILE} source/usr/bin/${BINARY_NAME}
rm source/usr/bin/.gitkeep || true
rm source/var/lib/passbeyond/.gitkeep || true

# Set permissions
chmod 755 source/DEBIAN -R
chmod +x source/DEBIAN/postinst source/DEBIAN/postrm source/DEBIAN/prerm
chmod +x source/usr/bin/${BINARY_NAME}
chmod 644 source/lib/systemd/system/*.service
# Create checksums
cd source/
find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums
cd ..
# Create package
dpkg-deb -z9 -Zxz -b source/ "${PKGNAME}_${NEWVERSION}_${BINARY_ARCH}.deb"
mkdir -p build/
mv *.deb build/
